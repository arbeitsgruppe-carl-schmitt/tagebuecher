define build_macro
for id in ${__cst_source_repo_ids}; do \
	site_subdir=$$(jq --raw-output --arg id "$${id}" '.builds[] | select(.id == $$id) | .site_subdir' <<<'${__cst_config_data}';); \
	echo "# Main Makefile. Calling sub-Makefile to build target ${1}_$${id} for sub-repo id=$${id} with site_subdir=$${site_subdir}"; \
	make ${1}_$${id} -j -f ${__cst_repo_path}/mk/process-repository.mk \
	id=$${id} \
	site_subdir=$${site_subdir} & \
done; \
wait;
endef