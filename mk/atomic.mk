clean: clean-local
	rm -rf machine-data
clean-local:
	rm -rf \
	build_components/11ty/subsites \
	build_components/11ty/_data/icons.json \
	build_components/11ty/_data/versions_index.json \
	build_components/11ty_print/subsites \
	machine-data/site \
	machine-data/xml-results
index-clean:
	rm -rf xml/entities/machine-data xml/entities/temp
index-restore:
	cd xml/entities && git restore .
install:
	sh/install.sh
pull:
	git pull;
	[[ -d xml/entities ]] && git -C xml/entities rev-parse 2>/dev/null && cd xml/entities && git checkout -q master;
	git submodule update --recursive --remote --rebase;
	[[ -d ../internal-xml ]] && git -C ../internal-xml rev-parse 2>/dev/null && cd ../internal-xml && git pull;
replace-entity-id:
	sh/replace-entity-id.sh old-id="${old-id}" new-id="$(new-id)" type="$(type)"
simplify-source-code:
	sh/simplify-source-code.sh target=${target}
sync:
	sh/sync-source-data.sh
uninstall:
	rm -rf site_components/fonts site_components/imgs