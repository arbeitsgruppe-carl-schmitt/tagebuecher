.ONESHELL:
.SILENT:

view := page

export __cst_books := $(shell jq --raw-output \
  '.builds[] | select(.id=="${id}") | .book_list[] | .book_signature' \
  <<<'${__cst_config_data}')

book_pdf_page_view_files        := $(patsubst %,${__cst_website_dir}${site_subdir}/pdf/%_page-view.pdf,${__cst_books})
book_print_html_page_view_files := $(patsubst %,${__cst_website_dir}${site_subdir}/print-html/%_page-view/index.html,${__cst_books})

json_comment_files := $(patsubst %,build_components/11ty/subsites/${id}/_includes/%_comments_${view}-view.json.liquid,${__cst_books})

define assemble_book_rule
${__cst_xml_results_dir}/${id}/${1}/${1}.xml: xml/${id}/${1}/*.xml
	echo "## Sub Makefile. ${id}. Assembling book $(1) XML source code"
	sh/assemble-book.sh ${id} $(1)
endef
$(foreach B,$(__cst_books),$(eval $(call assemble_book_rule,$B)))

xml_page-view_${id}: ${__cst_xml_results_dir}/${id}/edition/cst-edition_page-view.xml
	:

${__cst_xml_results_dir}/${id}/edition/cst-edition_page-view.xml: \
	$(foreach B,$(__cst_books),${__cst_xml_results_dir}/${id}/${B}/${B}.xml) \
	xml/${id}/edition/edition.xml
	echo "## Sub Makefile. ${id}. Creating XML Page View step 0"
	sh/process-repository.sh id=${id} target=xml_page-view_0
	echo "## Sub Makefile. ${id}. Creating XML Page View step 1"
	java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
	  -s:"${__cst_xml_results_dir}/${id}/edition/cst-edition_page-view_0.xml" \
	  -xsl:"build_components/xslt-1-xml/2b-process-main-file.xslt" \
	  -o:"${__cst_xml_results_dir}/${id}/edition/cst-edition_page-view_1.xml"
	echo "## Sub Makefile. ${id}. Creating XML Page View step 2"
	java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
	  -s:"${__cst_xml_results_dir}/${id}/edition/cst-edition_page-view_1.xml" \
	  -xsl:"build_components/xslt-1-xml/2c-process-main-file.xslt" \
	  -o:"${__cst_xml_results_dir}/${id}/edition/cst-edition_page-view.xml"

editorial-comments_${id}: ${json_comment_files}
	:

${json_comment_files} &: machine-data/json/${id}/editorial-comments.json
	echo "## Sub Makefile. ${id}. Creating JSON editorial comment files for ${view} view"
	sh/process-repository.sh id=${id} target=json-comment-files

machine-data/json/${id}/editorial-comments.json: \
  ${__cst_xml_results_dir}/${id}/edition/cst-edition_page-view.xml
	echo "## Sub Makefile. ${id}. Collecting editorial comments in JSON file"
	[[ -d machine-data/json/${id} ]] || mkdir -p machine-data/json/${id}
	java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
	  -s:"${__cst_xml_results_dir}/${id}/edition/cst-edition_page-view.xml" \
	  -xsl:"build_components/xslt-2-html/extract-editorial-notes.xslt" \
	  -o:"machine-data/json/${id}/editorial-comments.json"
	jq '.' machine-data/json/${id}/editorial-comments.json \
	  > machine-data/json/${id}/editorial-comments_tmp.json
	mv machine-data/json/${id}/editorial-comments_tmp.json \
	   machine-data/json/${id}/editorial-comments.json


print-html_$(id): ${book_print_html_page_view_files}
	:
${book_print_html_page_view_files} &: build_components/xslt-2-html/create-book_print_page-view.xslt
	echo "## Sub Makefile. ${id}. Creating Print HTML files"
	sh/process-repository.sh id=${id} site_subdir=${site_subdir} target=print-html

${__cst_website_dir}${site_subdir}/pdf/%_page-view.pdf: \
  ${__cst_website_dir}${site_subdir}/print-html/%_page-view/index.html \
  | prepare-content-dirs
	echo "## Sub Makefile. ${id}. Printing PDF file for book $*"
	prince http://cst.localhost${site_subdir}/print-html/$*_page-view/index.html \
	  --log=machine-data/logs/prince_${id} \
	  -o $@

prepare-content-dirs:
	[[ -d ${__cst_website_dir}${site_subdir}/pdf ]] || \
	  mkdir -p ${__cst_website_dir}${site_subdir}/pdf
	[[ -d machine-data/logs ]] || mkdir -p machine-data/logs
	[[ -d ${__cst_website_dir}${site_subdir}/xml ]] || \
	  mkdir -p ${__cst_website_dir}${site_subdir}/xml

# ------------------------------------------------------------
# Persons.json from the final page-view
build_components/11ty/subsites/${id}/_data/persons.json: \
  ${__cst_xml_results_dir}/${id}/entities-maps/persons.xml
	echo "## Sub Makefile. ${id}. Transforming persons' map to JSON"
	java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
	  -s:"${__cst_xml_results_dir}/${id}/entities-maps/persons.xml" \
	  -xsl:"build_components/xslt-1-xml/4b_transform-listPerson-to-json.xslt" \
	  -o:"${__cst_xml_results_dir}/${id}/entities-maps/persons.json"
	cat ${__cst_xml_results_dir}/${id}/entities-maps/persons.json | jq | \
	  sponge ${__cst_xml_results_dir}/${id}/entities-maps/persons.json
	[[ -d build_components/11ty/subsites/${id}/_data ]] || \
	  mkdir -p build_components/11ty/subsites/${id}/_data
	[[ -L ${__cst_repo_path}/build_components/11ty/subsites/${id}/_data/persons.json ]] || \
	  ln -s ${__cst_repo_path}/${__cst_xml_results_dir}/${id}/entities-maps/persons.json \
	        ${__cst_repo_path}/build_components/11ty/subsites/${id}/_data/persons.json

${__cst_xml_results_dir}/${id}/entities-maps/persons.xml: \
  ${__cst_xml_results_dir}/${id}/edition/cst-edition_page-view.xml
	echo "## Sub Makefile. ${id}. Creating XML persons' map"
	[[ -d ${__cst_xml_results_dir}/${id}/entities-maps ]] || \
	  mkdir -p ${__cst_xml_results_dir}/${id}/entities-maps
	java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
	  -s:"${__cst_xml_results_dir}/${id}/edition/cst-edition_page-view.xml" \
	  -xsl:"build_components/xslt-1-xml/4_create-entities-map.xslt" \
	  -o:"${__cst_xml_results_dir}/${id}/entities-maps/persons.xml" \
	  entity=person \
	  entities_list_path=${__cst_repo_path}/xml/entities/persons.xml

# ------------------------------------------------------------
# Build main site
site_0_${id}: ${__cst_website_dir}${site_subdir}/index.html
	:

${__cst_website_dir}${site_subdir}/index.html: \
  xml_page-view_${id} \
  build_components/11ty/*.liquid \
  build_components/11ty/_includes/*.liquid \
  editorial-comments_$(id) \
  build_components/11ty/subsites/${id}/_data/persons.json \
  ${book_pdf_page_view_files}
	echo "## Sub Makefile ${id}. Creating website step 0"
	sh/process-repository.sh id=${id} site_subdir=${site_subdir} target=site_0

# ------------------------------------------------------------
# “Archive” target
archive: \
  xml/${id}/RW-*/*.xml \
  xml/${id}/edition/edition.xml \
  ${__cst_website_dir}${site_subdir}/index.html

xml/${id}/RW-*/*.xml:
	{
	  [[ -d xml/${id} ]] || mkdir xml/${id}
	  rsync -rL --delete xml/${source_repo}/ xml/${id}/
	}

.PHONY: \
  archive \
  editorial-comments_${id} \
  pdf \
  prepare-pdf-dirs \
  print-html_$(id) \
  site_0_${id} \
  xml_page-view_$(id)
