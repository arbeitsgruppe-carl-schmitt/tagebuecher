MAKEFLAGS += --no-print-directory
MAKEFLAGS += -j

include mk/variables.mk
include mk/atomic.mk
include mk/macros.mk

machine-data/zotero-bibliography/zoteroDB.json:
	@if [[ ! -f machine-data/zotero-bibliography/zoteroDB.json || ! $$(find machine-data/zotero-bibliography/zoteroDB.json -newermt "24 hours ago") ]]; then \
	echo "# Main Makefile. Downloading \"Carl Schmitt Tagebücher\" Library from Zotero ${__cst_zotero_url}"; \
	sh/update-zoteroDB.sh; \
	fi;
machine-data/zotero-bibliography/zotero-bibliography.xml: machine-data/zotero-bibliography/zoteroDB.json
	@echo "# Main Makefile. Transforming Zotero JSON data into TEI XML"; \
	java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
	-it:"init" \
	-xsl:"${__cst_repo_path}/build_components/xslt-1-xml/4j_transform-zoteroDB-to-xml.xslt" \
	-o:"${__cst_repo_path}/machine-data/zotero-bibliography/zotero-bibliography.xml" \
	json-file-path="${__cst_repo_path}/machine-data/zotero-bibliography/zoteroDB.json"	
xml_page-view: machine-data/zotero-bibliography/zotero-bibliography.xml
	@$(call build_macro,xml_page-view)
index-xml:
	@echo "# Main Makefile. Updating entities' index"; \
	sh/update-index.sh
xml_simple-view:
	@$(call build_macro,xml_simple-view)
11ty: build_components/11ty/_data/icons.json \
	build_components/11ty/_data/versions_index.json
	@:
build_components/11ty/_data/icons.json build_components/11ty/_data/versions_index.json &: \
build_components/11ty/_data/free-icons.json \
build_components/11ty/_data/proprietary-icons.json xml_page-view
	@echo "# Main Makefile. Setting up 11ty Page Builder"; \
	sh/set-up-11ty.sh
${__cst_repo_path}/${__cst_website_dir}/index.html:	index-xml 11ty site_components/*
	@[[ -d ${__cst_repo_path}/${__cst_website_dir} ]] \
	|| mkdir -p ${__cst_repo_path}/${__cst_website_dir}; \
	[[ -L ${__cst_repo_path}/${__cst_website_dir}/fonts ]] \
	|| ln -sd ${__cst_repo_path}/site_components/fonts ${__cst_repo_path}/${__cst_website_dir}/fonts; \
	$(call build_macro,site_0)
site: ${__cst_repo_path}/${__cst_website_dir}/index.html
	@:
archive: 11ty
	sh/build.sh target=archive
release: ${__cst_repo_path}/${__cst_website_release_dir}/index.html
${__cst_repo_path}/${__cst_website_release_dir}/index.html: ${__cst_repo_path}/${__cst_website_dir}/index.html
	@echo "# Main Makefile. Creating release"; \
	sh/create-release.sh

.PHONY: 11ty clean index-clean index-restore install pull release simplify-source-code site site_0 sync uninstall xml_page-view xml_simple-view
