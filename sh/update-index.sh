#!/bin/bash

add_authors_to_persons() {
	local works_list_file="$gnd_index_result_path/machine-data/works.list"
	local works_dataset_folder="$gnd_index_result_path/machine-data/works-datasets"
	local authors_list_file="$gnd_index_result_path/machine-data/authors.list"
	for file in $authors_list_file ${authors_list_file}_1; do
		[[ ! -f "$file" ]] || rm "$file"
	done
	while read line; do
		work_dataset="$works_dataset_folder/$line.xml"
		[[ -f "$works_dataset_folder/$line.xml" ]] && \
		gnd_url=$(xmlstarlet sel -t -v '//gndo:firstAuthor//rdf:Description/@rdf:about' "$work_dataset")
		[[ "$gnd_url" =~ ^https://d-nb.info/gnd/[0-9X-]*$ ]] && \
		echo ${gnd_url#https://d-nb.info/gnd/} >>"${authors_list_file}_1"
	done <"$works_list_file"
	[[ -f "${authors_list_file}_1" ]] || return 0
	cat "${authors_list_file}_1" | sort | uniq -u >"$authors_list_file"
	ids_arr=($(cat "$authors_list_file"))
	fetch_gnd_datasets "$gnd_index_result_path/machine-data/persons-datasets" ${ids_arr[@]}
	local authors_arr=($(cat "$authors_list_file"))
	printf -v authors_joined '%s,' "${authors_arr[@]}"
	java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
	-s:"$gnd_index_result_path/temp/persons_1.xml" \
	-xsl:"${__cst_repo_path}/build_components/xslt-1-xml/3a3-add-authors-to-persons.xslt" \
	-o:"$gnd_index_result_path/temp/persons_2.xml" \
	authors-list=${authors_joined%,}
	mv "$gnd_index_result_path/temp/persons_2.xml" "$gnd_index_result_path/temp/persons_1.xml"
}

create_xml_datasets() {
	local entity="$1"
	local gnd_entities_list_path="$gnd_index_result_path/machine-data/${entity}s.list"
	local entities_file="$gnd_index_result_path/${entity}s.xml"
	local entities_file_temp="$gnd_index_result_path/temp/${entity}s_1.xml"
	if [[ -f "$entities_file" ]]; then
		cp "$entities_file" "${entities_file_temp}_old"
		local entities_file_old="${entities_file_temp}_old"
	else
		local entities_file_old="none"
	fi
	local datasets_path="$gnd_index_result_path/machine-data/${entity}s-datasets"
	[[ -d "datasets_path" ]] || mkdir -p "$datasets_path"
	java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
	-s:"${__cst_repo_path}/${__cst_xml_results_dir}/${__cst_index_source_repo}/edition/cst-edition_page-view.xml" \
	-xsl:"${__cst_repo_path}/build_components/xslt-1-xml/3a1-update-entities-index.xslt" \
	-o:"${entities_file_temp}" \
	entity=$entity \
	entities-config-data-path="$entities_config_data_path" \
	entities-file-old="$entities_file_old" \
	gnd-entities-list-path="$gnd_entities_list_path"
}

enrich_xml_datasets() {
	local entity="$1"
	local entities_file_temp="$gnd_index_result_path/temp/${entity}s_1.xml"
	java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
	-s:"$entities_file_temp" \
	-xsl:"${__cst_repo_path}/build_components/xslt-1-xml/3a2-enrich-entities-index.xslt" \
	-o:"$gnd_index_result_path/${entity}s.xml" \
	entity=$entity \
	entities-data-path="$gnd_index_result_path/machine-data/${entity}s-datasets" \
	persons-entities-file-path="$gnd_index_result_path/persons.xml"
}

fetch_gnd_datasets() {
	local target_path="$1"
	[[ -d "$target_path" ]] || mkdir -p "$target_path"
	shift
	local entities_arr=($@)
	local entities_download_arr=()
	for id in ${entities_arr[@]}; do
		if [[ ! -f "$target_path/$id.xml" || $(find "$target_path/$id.xml" -mtime +1) ]]; then
			entities_download_arr+=($id)
		fi
	done
	[[ ! ${entities_download_arr[@]} ]] || \
	parallel --jobs ${__cst_max_parallel_jobs} \
	"echo \"curl https://d-nb.info/gnd/{1}\"; \
	curl -s -L --header \"Accept: application/rdf+xml\" https://d-nb.info/gnd/{1} >$target_path/{1}.xml" \
	::: ${entities_download_arr[@]}
}

gnd_index_result_path="${__cst_repo_path}/xml/entities"
entities_config_data_path="$gnd_index_result_path/config/config.xml"
[[ -f "$entities_config_data_path" ]] || { echo "Entities config file not found. Exiting.."; exit 1; }
[[ -d "$gnd_index_result_path/temp" ]] || mkdir -p "$gnd_index_result_path/temp"

for entity in person place work; do
	{
		create_xml_datasets $entity;
		arr=($(cat "$gnd_index_result_path/machine-data/${entity}s.list"))
		fetch_gnd_datasets "$gnd_index_result_path/machine-data/${entity}s-datasets" ${arr[@]} 
	} &
done
wait
add_authors_to_persons
enrich_xml_datasets person
for entity in place work; do
	enrich_xml_datasets $entity &
done
wait
