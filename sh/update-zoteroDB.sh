#!/bin/bash
set -euo pipefail

SECONDS=0

limit=100
start=0
total=0

combined_file="${__cst_repo_path}/machine-data/zotero-bibliography/combined.json"
[[ -f $combined_file ]] && rm -f $combined_file
final_file=${__cst_repo_path}/machine-data/zotero-bibliography/zoteroDB.json
[[ -d ${__cst_repo_path}/machine-data/zotero-bibliography ]] || mkdir -p ${__cst_repo_path}/machine-data/zotero-bibliography

echo "Downloading \"Carl Schmitt Tagebücher\" Library from Zotero ${__cst_zotero_url}"

while :; do
    response=$(curl -sS -H "Accept: application/json" "${__cst_zotero_url}&limit=${limit}&start=${start}")
    count=$(echo "$response" | jq '. | length')
    total=$((total + count))
    echo "$response" | jq -c '.' >>$combined_file
    if [[ $count -lt $limit ]]; then
        break
    fi
    start=$((start + limit))
    echo -n "."
done

jq -cs 'add' $combined_file >$final_file
echo " Fetched $total items from Zotero and wrote into file ${final_file} in $SECONDS seconds."