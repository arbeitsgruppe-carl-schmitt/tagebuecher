#!/bin/bash
set -euo pipefail

script_dir="$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"
repo_dir="${script_dir%/sh}"
[[ $(cat $script_dir/build-config.json | jq --raw-output '.internal_repo_url') ]] || { echo "Nothing to do. Internal repository is not defined."; exit 0;  }
cd "$repo_dir"
if [[ $(git rev-parse --abbrev-ref HEAD) != "master" ]]; then
	echo "Error. This sync script will only work when you checked out the master branch. Run 'git checkout master' before continuing."
	exit 1
fi

public_xml_dir="$repo_dir/xml/public"
internal_xml_dir="$repo_dir/../internal-xml"
for dir in "$internal_xml_dir" "$public_xml_dir"; do
	if [[ ! -d "$dir" ]] || ! git -C "$dir" rev-parse 2>/dev/null; then
		echo "Error. Directory $dir is either not a directory or not a git repository. Exiting.."
		exit 1
	fi
done

####################################
#   Update public XML repository   #
####################################

echo "Updating local public repository"
cd "$public_xml_dir"
git checkout --quiet master
git pull --quiet

######################################
#   Update internal XML repository   #
######################################

echo "Updating local internal repository"
cd $internal_xml_dir
git checkout master --quiet
git pull --quiet
datasets=( $(cat $script_dir/build-config.json | \
	jq --raw-output '.builds[] | select(.include==true) | select(.source_repo=="public") | .book_list[].book_signature') )
source_files=()
for dataset in ${datasets[@]}; do
	source_files+=("$public_xml_dir/$dataset")
done
source_files+=("$public_xml_dir/drafts" "$public_xml_dir/edition" "$public_xml_dir/author-mode.css" "$public_xml_dir/empty-skeleton-header.xml")
rsync -av --exclude='*_main.xml' ${source_files[@]} $internal_xml_dir
[[ ! $(git status --porcelain) ]] || { git commit -a -m "Update from public repository"; git push; }

