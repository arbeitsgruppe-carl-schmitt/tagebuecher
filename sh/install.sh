#!/bin/bash
set -euo pipefail

install_data_pkg() {
	local pkg="$1"
	[[ -d "${__cst_repo_path}/$pkg" ]] && { echo "Package $pkg is already installed."; return 0; }
	echo "Installing $pkg from ${data_pkgs[${pkg}]}"
	mkdir -p ${__cst_repo_path}/$pkg
	curl "${data_pkgs[${pkg}]}" | tar -xz --no-same-owner --directory="${__cst_repo_path}/$pkg"
}

fonts_url="https://filedn.eu/lnnyiBBWb80fqcB0MmuQFXm/cst/fonts.tar.gz"
img_url="https://filedn.eu/lnnyiBBWb80fqcB0MmuQFXm/cst/imgs.tar.gz"
declare -A data_pkgs=( \
	[site_components/fonts/fontawesome6/free]="https://filedn.eu/lnnyiBBWb80fqcB0MmuQFXm/cst/fontawesome6-free.tar.gz" \
	[site_components/fonts/google-fonts]="https://filedn.eu/lnnyiBBWb80fqcB0MmuQFXm/cst/google-fonts.tar.gz" \
	[site_components/imgs]="https://filedn.eu/lnnyiBBWb80fqcB0MmuQFXm/cst/imgs.tar.gz" \
	)

fonts_path="${__cst_repo_path}/site_components/fonts"
img_path="${__cst_repo_path}/site_components/imgs"
[[ -f ${__cst_script_path}/build-config.json ]] || cp ${__cst_script_path}/build-config-sample.json ${__cst_script_path}/build-config.json 

# Set timezone to CEST (Europe/Berlin). Adjust to your needs, if necessary.
rm /etc/localtime
ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime

# Create links für nginx server
[[ -d /data/www ]] || mkdir -p /data/www
[[ -L /data/www/cst ]] || ln -sd ${__cst_repo_path}/${__cst_website_dir} /data/www/cst
[[ -L /data/www/cst-release ]] || ln -sd ${__cst_repo_path}/machine-data/site_release /data/www/cst-release

# Install data packages
for pkg in ${!data_pkgs[@]}; do
	install_data_pkg $pkg
done

# Set up internal repo
internal_repo_path="${__cst_project_path}/internal-xml"
internal_repo_link="${__cst_repo_path}/xml/internal"
if [[ ! "${__cst_internal_repo_url}" == "null" ]] && ! git -C "$internal_repo_path" rev-parse 2>/dev/null; then
	[[ -L "$internal_repo_link" ]] || ln -sd "$internal_repo_path" "$internal_repo_link"
	rsync -a "${__cst_repo_path}/xml/public/edition" "$internal_repo_link"
fi

# Set up entities repo
entities_repo_path="${__cst_repo_path}/xml/entities"
[[ -d "$entities_repo_path" ]] && git -C "$entities_repo_path" rev-parse 2>/dev/null \
|| { echo "Error. Entities' repo not found. Exiting."; exit 1; }
[[ -f "$entities_repo_path/config.xml" ]] \
|| cp "$entities_repo_path/config/config_sample.xml" "$entities_repo_path/config/config.xml"
