#!/bin/bash

xslt_file_1="${__cst_repo_path}/build_components/xslt-1-xml/9a-normalize-source-code.xslt"
xslt_file_2="${__cst_repo_path}/build_components/xslt-1-xml/9b-normalize-source-code.xslt"

for arg in "$@"; do
	if [[ $arg == target=* ]]; then
		target_dir=${arg#target=}
	else
		echo "Error. Unrecognized argument: $arg."
		exit 1
	fi
done

[[ -d $target_dir ]] || { echo "Error. $target_dir is not a directory."; exit 1; }

cd $target_dir
xml_files="$(find RW-0265-* | grep .xml$ | grep -v main.xml$)"

parallel --jobs ${__cst_max_parallel_jobs} \
"echo \"Processing file {1}\"; \
java -cp ${__cst_saxon_path} net.sf.saxon.Transform \
-s:{1} \
-xsl:$xslt_file_1 \
-o:{1}_stage-1; \
java -cp ${__cst_saxon_path} net.sf.saxon.Transform \
-s:{1}_stage-1 \
-xsl:$xslt_file_2 \
-o:{1}_out; \
mv {1}_out {1}; \
rm -f {1}_stage-1;" \
::: ${xml_files}