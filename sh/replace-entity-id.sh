#!/bin/bash
set -euo pipefail

old_id=""
new_id=""
type=""

for arg in $@; do
	if [[ $arg == old-id=* ]]; then
		old_id="${arg#old-id=}"
	elif [[ $arg == new-id=* ]]; then
		new_id="${arg#new-id=}"
	elif [[ $arg == type=* ]]; then
		type=${arg#type=}
	fi
done

[[ $old_id ]] || { echo "Error. Old id has not been declared." exit 1; }
[[ $new_id ]] || { echo "Error. New id has not been declared." exit 1; }
[[ $type ]] || { echo "Error. Entity type has not been declared." exit 1; }

entities_file=${__cst_repo_path}/xml/entities/${type}s.xml
[[ -f $entities_file ]] || \
{ echo "Error. Entities file $entities_file not found."; exit 1;}
public_repo_path=${__cst_repo_path}/xml/public
if [[ ! ${__cst_index_source_repo} == 'public' ]]; then
	other_repo_path=${__cst_repo_path}/xml/${__cst_index_source_repo}
else
	other_repo_path=""
fi

sed -i "s| xml:id=\"${old_id}\"| xml:id=\"${new_id}\"|g;" $entities_file
sed -i "s| ref=\"#${old_id}\"| ref=\"#${new_id}\"|g;" $public_repo_path/RW*/*.xml
[[ $other_repo_path ]] && \
sed -i "s| ref=\"#${old_id}\"| ref=\"#${new_id}\"|g;" $other_repo_path/RW*/*.xml