#!/bin/bash
set -euo pipefail

copy_css() {
    for file in $(ls $site_path/$site_subdir/css); do {
        csso -i $site_path$site_subdir/css/$file -o ./css/$file
        gzip -c -f ./css/$file >./css/${file}.gz
    } &
done
}

copy_html() {
    while read -r line; do
        html_files+=($line)
    done <$site_path/$site_subdir/map
    for path in ${html_files[@]}; do {
        dir=$(dirname $path)
        [[ -d $dir ]] || mkdir $dir 
        html-minifier \
        --collapse-whitespace \
        --minify-css true \
        --minify-js true \
        --minify-urls true \
        --remove-comments \
        --trim-custom-fragments \
        $site_path$site_subdir/$path >$path
        gzip -c -f "$path" >"${path}.gz"
    } &
    done
}

copy_js() {
    for file in $(ls $site_path/$site_subdir/js); do {
        java -jar "${__cst_closure_path}" --js $site_path$site_subdir/js/$file --js_output_file ./js/$file
        gzip -c -f ./js/$file >./js/${file}.gz
        } &
    done
}

create_download_pkg() {
    [[ -d downloads/xml ]] || mkdir -p downloads/xml
    java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
    -s:${__cst_repo_path}/xml_results/$id/edition/cst-edition_page-view.xml \
    -xsl:${__cst_repo_path}/build_components/xslt-2-html/9-transform-result-xml-for-text-package.xslt \
    -o:downloads/xml/cst-edition_page-view.xml &
    cp ${__cst_repo_path}/xml/$id/author-mode.css downloads/xml/cst-oxy-author-mode.css &
    {
        echo -e "$(<${__cst_repo_path}/build_components/md_modules/edition-heading.md)\n" >downloads/README.md
        echo -e "$(<${__cst_repo_path}/build_components/md_modules/text-archive-info.md)\n" >>downloads/README.md
        echo -e "This archive has been build on $(date) and is based on commit $(
            cd ${__cst_repo_path}
            git rev-parse --short HEAD
            ) of the [source repository](https://gitlab.com/arbeitsgruppe-carl-schmitt/tagebuecher).\n" >>downloads/README.md
        echo -n "$(<${__cst_repo_path}/build_components/md_modules/text-archive-license-info.md)" >>downloads/README.md
    } &
    cd downloads/
    zip_paths=("README.md" "xml")
    wait
    zip -q -r cst-primary-text-package.zip ${zip_paths[@]} 
    rm -rf ${zip_paths[@]}
}

for arg in $@; do
    case $arg in
        id=*) id=${arg#id=} ;;
        site_subdir=*) site_subdir=${arg#site_subdir=} ;;
    esac
done

script_path="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
repo_path="${script_path%/sh}"
source ${__cst_script_path}/snippets/process-json-config-data/process-json-config-data.sh
config_data="$(cat ${__cst_script_path}/build-config.json)"
process_json_config_data closure_path saxon_path <<<"${__cst_config_data}"
site_path="${__cst_repo_path}/site"
release_path="${__cst_repo_path}/site_release"

echo "Creating release ${__cst_repo_path}$site_subdir"
[[ -d $release_path/$site_subdir ]] || mkdir -p $release_path/$site_subdir
cd $release_path$site_subdir
for folder in imgs; do
    cp -rL $site_path$site_subdir/$folder ./$folder &
done
for folder in css js; do
    [[ -d $folder ]] || mkdir $folder
done
copy_html &
copy_css &
copy_js &
create_download_pkg &
wait
