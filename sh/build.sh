#!/bin/bash
set -euo pipefail

case $1 in
	target=archive) target="archive" ;;
	*) echo "Error. Unknown argument: $1. Exiting."; exit 1; ;; 
esac

[[ $target ]] || { echo "Error. No build target specified. Exiting."; exit 1; } 

declare -i i=0
declare -i len=0
if [[ $target == "archive" ]]; then
	i=0
	len="$(jq '.archive | length' <<<"${__cst_config_data}")"
	cd "${__cst_repo_path}"
	while [[ $i -lt $len ]]; do
		map="$(jq ".archive[$i]" <<<"${__cst_config_data}")"
		((i+=1))
		current_branch="$(git rev-parse --abbrev-ref HEAD)"
		commit="$(jq --raw-output '.commit' <<<"$map")"
		cp -r "${__cst_repo_path}" "${__cst_project_path}/tagebuecher_safety-copy_$(date +%s)"
		git checkout --quiet $commit
		git submodule update
		make archive -f ${__cst_repo_path}/mk/process-repository.mk \
		id="$(jq --raw-output '.id' <<<"$map")" \
		site_subdir="$(jq --raw-output '.site_subdir' <<<"$map")" \
		source_repo="$(jq --raw-output '.source_repo' <<<"$map")"
		git checkout $current_branch
		git submodule update
	done
fi

