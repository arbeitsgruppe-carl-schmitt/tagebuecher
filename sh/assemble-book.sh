#!/bin/bash
set -euo pipefail

id="$1"
book="$2"
source_dir="${__cst_repo_path}/xml/${id}/${book}"
target_dir="${__cst_repo_path}/${__cst_xml_results_dir}/${id}/${book}"
xslt_script_path="${__cst_repo_path}/build_components/xslt-1-xml"

[[ -d "$source_dir" ]] || {
	echo "source dir $source_dir not found, exiting assemble book script"
	return
}
[[ -d "$target_dir" ]] || mkdir -p "$target_dir"

# Run transformations and write to temporary file
java -cp "${__cst_xerces_path}:${__cst_saxon_path}" net.sf.saxon.Transform -xi:on \
-s:${source_dir}/${book}_main.xml \
-xsl:${xslt_script_path}/1a-assemble-source-file.xslt \
-o:${target_dir}/$book-stage-1.xml
book_scan_info=$(jq -c --arg id "$id" --arg book "$book" '
  (.builds[]
    | select(.id == $id)
    | .book_list[]
    | select(.book_signature == $book))
  // {}
' <<<"${__cst_config_data}")
first_page=$(jq -r '.scan_urls?.first_page // ""' <<<"$book_scan_info")
page_increment=$(jq -r '.scan_urls?.page_increment // ""' <<<"$book_scan_info")
scan_link=$(jq -r '.scan_urls?.link // ""' <<<"$book_scan_info")
java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
-s:${target_dir}/${book}-stage-1.xml \
-xsl:${xslt_script_path}/1b-process-source-file.xslt \
-o:${target_dir}/${book}-stage-2.xml \
book=$book \
first-page=$first_page \
page-increment=$page_increment \
scan-link=$scan_link

cat ${target_dir}/${book}-stage-2.xml |
  gawk 'NF' |
  gawk '/-<\/c>$/ { printf("%s", $0); next; } 1' |
  sed 's|-</c>\s*|-</c>|g;' \
  >${target_dir}/${book}-stage-3.xml

java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
  -s:${target_dir}/${book}-stage-3.xml \
  -xsl:${xslt_script_path}/1c-process-source-file.xslt \
  -o:"${target_dir}/$book.xml"

# if ! cmp -s "${target_dir}/${book}.xml.tmp" "${target_dir}/${book}.xml"; then
#   mv "${target_dir}/${book}.xml.tmp" "${target_dir}/${book}.xml"
# else
#   rm "${target_dir}/${book}.xml.tmp"
# fi
