#!/bin/bash
set -euo pipefail

exclude_dirs='downloads|fonts|imgs|pdf|xml'

function gzip_file() {
    [[ -f $1 ]] && gzip -c -f $1 >${1}.gz;
}

function create_dirtree() {
    cd ${__cst_repo_path}/${__cst_website_dir}
    mapfile -t dirtree < <(find -L . -type d | sed 's/\.$//;s|^\./||g;')
    rm -rf ${__cst_repo_path}/${__cst_website_release_dir}
    mkdir ${__cst_repo_path}/${__cst_website_release_dir}
    cd ${__cst_repo_path}/${__cst_website_release_dir}
    mapfile -t dirs_to_create < <(printf '%s\n' "${dirtree[@]}" \
        | grep -v -E "(^|/)($exclude_dirs)(/|$)")
    for dir in ${dirs_to_create[@]}; do
        mkdir -p $dir
    done
    mapfile -t dirs_to_copy < <(printf '%s\n' "${dirtree[@]}" \
        | grep -E "(^|/)($exclude_dirs)$")
    for dir in ${dirs_to_copy[@]}; do
        cp -rL ${__cst_repo_path}/${__cst_website_dir}/$dir $dir &
    done
    wait
}

function copy_css_files() {
    mapfile -t css_files < <(printf '%s\n' "${filetree[@]}" | grep .css$)
    parallel --jobs ${__cst_max_parallel_jobs} "\
    if ! csso -i ${__cst_repo_path}/${__cst_website_dir}/{1} \
    -o {1}; then \
        echo \"CSS minification of {1} failed.\"; \
    fi; \
    [[ -f {1} ]] && gzip -c -f {1} >{1}.gz;" \
    ::: ${css_files[@]}
}

function copy_html_files() {
    mapfile -t html_files < <(printf '%s\n' "${filetree[@]}" | grep .html$)
    parallel --jobs ${__cst_max_parallel_jobs} "\
    if ! html-minifier \
        --collapse-whitespace \
        --minify-css true \
        --minify-js true \
        --minify-urls true \
        --remove-comments \
        --trim-custom-fragments \
        ${__cst_repo_path}/${__cst_website_dir}/{1} -o {1}; then \
        echo \"HTML minification of ${__cst_repo_path}/${__cst_website_dir}/{1} failed.\"; \
    fi; \
    [[ -f {1} ]] && gzip -c -f {1} >{1}.gz;" \
    ::: ${html_files[@]}
}

function copy_js_files() {
    mapfile -t js_files < <(printf '%s\n' "${filetree[@]}" | grep .js$)
    parallel --jobs ${__cst_max_parallel_jobs} "\
    if ! java -jar ${__cst_closure_path} \
    --js ${__cst_repo_path}/${__cst_website_dir}/{1} \
    --js_output_file {1}; then \
    echo \"JS minification of {1} failed.\"; \
    fi; \
    [[ -f {1} ]] && gzip -c -f {1} >{1}.gz;" \
    ::: ${js_files[@]}
}

cd ${__cst_repo_path}/${__cst_website_dir}
create_dirtree &
mapfile -t filetree < <(find -L . -type f | grep -v -E "(^|/)($exclude_dirs)(/|$)" | sed 's|^\./||g')
wait
cd ${__cst_repo_path}/${__cst_website_release_dir}
copy_css_files &
copy_html_files &
copy_js_files &
wait
