#!/bin/bash

set -euo pipefail
shopt -s expand_aliases

create_download_pkg() {
    cd $html_path
    [[ -d downloads/xml ]] || mkdir -p downloads/xml
    java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
    -s:${__cst_repo_path}/${__cst_xml_results_dir}/$id/edition/cst-edition_page-view.xml \
    -xsl:${__cst_repo_path}/build_components/xslt-2-html/9-transform-result-xml-for-text-package.xslt \
    -o:downloads/xml/cst-edition_page-view.xml &
    cp ${__cst_repo_path}/xml/$id/author-mode.css downloads/xml/cst-oxy-author-mode.css &
    {
        echo -e "$(<${__cst_repo_path}/build_components/md_modules/edition-heading.md)\n" >downloads/README.md
        echo -e "$(<${__cst_repo_path}/build_components/md_modules/text-archive-info.md)\n" >>downloads/README.md
        echo -e "This archive has been build on $(date) and is based on commit $(
            cd ${__cst_repo_path}
            git rev-parse --short HEAD
            ) of the [source repository](https://gitlab.com/arbeitsgruppe-carl-schmitt/tagebuecher).\n" >>downloads/README.md
        echo -n "$(<${__cst_repo_path}/build_components/md_modules/text-archive-license-info.md)" >>downloads/README.md
    } &
    cd downloads/
    zip_paths=("README.md" "xml")
    wait
    zip -q -r cst-primary-text-package.zip ${zip_paths[@]} 
    rm -rf ${zip_paths[@]}
}

create_pdf() {
    book=$1
    pdf_dir=${__cst_repo_path}/${__cst_website_dir}${site_subdir}/pdf
    [[ -d $pdf_dir ]] \
    || mkdir $pdf_dir
}

create_print_html() {
        local book="$1"
        local liquid_content_file="${__cst_repo_path}/build_components/11ty_print/subsites/${id}/_includes/${book}_page-view.html.liquid"
        [[ -d "$(dirname "$liquid_content_file")" ]] || mkdir -p "$(dirname "$liquid_content_file")"
        local xslt_file="${__cst_repo_path}/build_components/xslt-2-html/create-book_print_page-view.xslt"
        java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
        -s:"$xml_dir/edition/cst-edition_page-view.xml" \
        -xsl:"$xslt_file" \
        -o:"$liquid_content_file" \
        book-id=${book}
        local liquid_file="${__cst_repo_path}/build_components/11ty_print/subsites/${id}/${book}_page-view.liquid"
        echo \
"---
layout: standard.liquid
---
{% include \"_includes/${book}_page-view.html.liquid\" %}
" >"$liquid_file"
}

parse_targets() {
    target_str=$1
    build_json_comment_files="false"
    build_site_0="false"
    build_pdf="false"
    build_print_html="false"
    build_source_xml="false"
    build_xml="false"
    build_xml_page_view="false"
    IFS_old="$IFS"
    IFS=',' read -r -a target_arr <<<$target_str
    IFS="$IFS_old"
    for target in ${target_arr[@]}; do
        case $target in
        json-comment-files) build_json_comment_files="true" ;;
        site_0) build_site_0="true" ;;
        print-html) build_print_html="true" ;;
        pdf) build_pdf="true" ;;
        index) : ;;
        xml_page-view_0) build_xml_page_view="true" ;;
        xml) : ;;
        *)
            echo "Unknown target: $target. Exiting"
            exit 1
            ;;
        esac
    done
    if [[ $build_json_comment_files == "false" && $build_xml_page_view == "false" && $build_site_0 == "false" && $build_print_html == "false" && $build_pdf == "false" ]]; then
        echo "No build targets specified. Nothing to do."
        exit 0
    fi
}

#############
# Main part #
#############

is_archive="false"
for arg in $@; do
    case $arg in
    id=*) id=${arg#id=} ;;
    target=*) parse_targets ${arg#target=} ;;
    site_subdir=*) site_subdir=${arg#site_subdir=} ;;
    *)
        echo "Unknown argument: $arg. Exiting"
        exit 1
        ;;
    esac
done
[[ $id ]] || {
    echo "No source data defined. Nothing to do."
    exit 0
}
books=( ${__cst_books} )
[[ "${books[@]}" ]] || {
    echo "No books to process in source $id. Nothing to do."
    exit 0
}

xml_dir="${__cst_repo_path}/${__cst_xml_results_dir}/${id}"

##########################
# Code for build targets #
##########################

if [[ $build_json_comment_files == "true" ]]; then
    view=page
    target_dir=${__cst_repo_path}/build_components/11ty/subsites/${id}/_includes
    [[ -d "$target_dir" ]] || mkdir -p "$target_dir"
    for book in ${books[@]}; do {
        json_keys="$(java -cp "${__cst_saxon_path}" net.sf.saxon.Query \
            -s:"${__cst_repo_path}/machine-data/xml-results/${id}/edition/cst-edition_${view}-view.xml" \
            -qs:"declare namespace tei='http://www.tei-c.org/ns/1.0'; \
            declare option saxon:output 'method=text'; \
            data(//tei:TEI[@xml:id='$book']/tei:text//tei:app/tei:note/@xml:id)" \
            | jq -R 'split(" ") | map(select(length > 0))')";
        if [ -z "$json_keys" ]; then
            json_keys="[]"
        fi
        jq --argjson keys "$json_keys" 'with_entries(select(.key as $k | $keys | index($k)))' \
        "${__cst_repo_path}/machine-data/json/internal/editorial-comments.json" >"$target_dir/${book}_comments_${view}-view.json.liquid"; } &
    done
    wait
fi

if [[ $build_xml_page_view == "true" ]]; then
    xslt_script_path="${__cst_repo_path}/build_components/xslt-1-xml"
    [[ -d "${xml_dir}/edition" ]] || mkdir -p "${xml_dir}/edition"
    cp ${__cst_repo_path}/xml/$id/author-mode.css $xml_dir/

    book_paths=""
    limit=$((${#books[@]} - 1))
    i=$((0))
    for book in ${books[@]}; do
        book_paths+="${xml_dir}/${book}/${book}.xml"
        [[ $i -lt $limit ]] && book_paths+=','
        ((i += 1))
    done
    java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
        -s:${__cst_repo_path}/xml/${id}/edition/edition.xml \
        -xsl:${xslt_script_path}/2a-integrate-components-into-main-file.xslt \
        -o:${xml_dir}/edition/cst-edition_page-view_0.xml \
        bookUris="$book_paths" \
        zotero_bibliography_path="${__cst_repo_path}/${__cst_zotero_bibliography_path}"
fi

if [[ $build_site_0 == "true" ]]; then
    xslt_script_path="${__cst_repo_path}/build_components/xslt-2-html"
    eleventy_path="${__cst_repo_path}/build_components/11ty"
    eleventy_sub_path="${__cst_repo_path}/build_components/11ty/subsites/$id"
    files_to_exclude=($(jq --raw-output '.exclude_from_public_site[]' <<<"${__cst_config_data}"))
    html_path="${__cst_repo_path}/${__cst_website_dir}${site_subdir}"
    [[ -d $html_path ]] || mkdir -p $html_path
    html_content_path="$eleventy_sub_path/_includes"

    cd $eleventy_path
    eleventy_sourcefiles=($(ls | grep .liquid$ | grep -v ^placeholder.liquid$))
    eleventy_sourcefiles+=(_data/icons.json _data/versions_index.json _data/persons.json)
    for file in $(ls _includes); do
        eleventy_sourcefiles+=(_includes/$file)
    done

    cd $eleventy_sub_path
    [[ -d _data ]] || mkdir _data
    [[ -d _includes ]] || mkdir _includes
    for file in ${eleventy_sourcefiles[@]}; do
        if [[ $id == "public" && $(echo ${files_to_exclude[@]} | grep -Fw ${file%.*} ) ]]; then
            rm -f $file
            ln -s "$eleventy_path/placeholder.liquid" $file
        else
            [[ -L $file ]] || ln -s "$eleventy_path/$file" $file
        fi
    done

    jq --arg _id $id '.versions[] | select(.id==$_id) |
    {"build_date": .build_date, "build_time": .build_time, "dir": .site_subdir, "label": .version_label, "number": .version_number}' <$eleventy_path/_data/versions_index.json \
    >$eleventy_sub_path/_data/version.json

    java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
        -s:$xml_dir/edition/cst-edition_page-view.xml \
        -xsl:$xslt_script_path/create-index-content.xslt \
        -o:$html_content_path/index-content.liquid &
    for book in ${books[@]}; do
        java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
            -s:$xml_dir/edition/cst-edition_page-view.xml \
            -xsl:$xslt_script_path/create-book_screen_page-view.xslt \
            -o:$html_content_path/${book}_page-view.liquid \
            book-id=${book} &      
        java -cp "${__cst_saxon_path}" net.sf.saxon.Transform \
            -s:$xml_dir/$book/$book.xml \
            -xsl:$xslt_script_path/create-book-editorial.xslt \
            -o:$html_content_path/${book}_editorial.liquid &
        view=page
        comments_path="_includes/${book}_comments_${view}-view.json.liquid"
        pdf_page_view_path="pdf/${book}_page-view.pdf"
        pdf_page_view_size=$(du -h ${__cst_repo_path}/${__cst_website_dir}${site_subdir}/${pdf_page_view_path} | cut -f1)
        xml_page_view_path=xml/${book}_page-view.xml
        xmlstarlet sel -N tei="http://www.tei-c.org/ns/1.0" \
        -t -c "/tei:teiCorpus/tei:TEI[@xml:id='${book}']" \
        $xml_dir/edition/cst-edition_page-view.xml \
        >${__cst_repo_path}/${__cst_website_dir}${site_subdir}/${xml_page_view_path}
        xml_page_view_size=$(du -h ${__cst_repo_path}/${__cst_website_dir}${site_subdir}/${xml_page_view_path} | cut -f1)
        cp "$eleventy_path/templates/book.liquid" "$eleventy_sub_path/$book.liquid"
        js_page_name="$(sed 's/-/_/g;' <<<$book)_Page"
        sed -i "\
        0,/book_id:/{s//book_id: $book/}; \
        0,/comments_path:/{s||comments_path: ${comments_path}|}; \
        0,/pdf_page_view_path:/{s||pdf_page_view_path: ${pdf_page_view_path}|}; \
        0,/pdf_page_view_size:/{s||pdf_page_view_size: ${pdf_page_view_size}|}; \
        0,/title:/{s//title: $book/}; \
        0,/xml_page_view_path:/{s||xml_page_view_path: ${xml_page_view_path}|}; \
        0,/xml_page_view_size:/{s||xml_page_view_size: ${xml_page_view_size}|}; \
        0,/js_page_name:/{s//js_page_name: $js_page_name/}; \
        0,/book_editorial.liquid/{s//${book}_editorial.liquid/}; \
        0,/book_page-view.liquid/{s//${book}_page-view.liquid/};" "$eleventy_sub_path/$book.liquid" &
    done
    wait

    eleventy --input="$eleventy_sub_path" --output="$html_path" --quiet &
    create_download_pkg &
    symlinks=(css js imgs)
    for link in ${symlinks[@]}; do
        [[ -L "${html_path}/${link}" ]] || ln -sd "${__cst_repo_path}/site_components/${link}" "${html_path}/${link}" &
    done
    wait
fi

if [[ $build_print_html == "true" ]]; then
    for book in ${books[@]}; do
        create_print_html $book &
    done
    wait
    el_project_path="${__cst_repo_path}/build_components/11ty_print"
    el_target_path="${__cst_repo_path}/${__cst_website_dir}${site_subdir}/print-html"
    cd "$el_project_path/subsites/${id}"
    files=( "_includes/standard.liquid" )
    for f in ${files[@]}; do
        [[ -L $f ]] || ln -s "$el_project_path/$f" $f
    done
    eleventy --input="$el_project_path/subsites/$id" --output="$el_target_path" --quiet
    cp "$el_project_path/print.css" "$el_target_path/" 
fi
