#!/bin/bash
set -euo pipefail
shopt -s expand_aliases

eleventy_path="${__cst_repo_path}/build_components/11ty"
data_path="$eleventy_path/_data"
[[ -d "$data_path" ]] || mkdir -p "$data_path"
cd "$data_path"
icons_file=icons.json

# Update icons.json only if content changes
if [[ ${__cst_use_fontawesome_pro} == "true" ]]; then
    new_icons_file="proprietary-icons.json"
else
    new_icons_file="free-icons.json"
fi

if ! cmp -s "$new_icons_file" "$icons_file"; then
    cp "$new_icons_file" "$icons_file"
fi

array="$(jq '[ .builds | .[] | select(.include==true) |
    if .commit? then .["is_stable"]=true | .["main_repo_commit"]=.commit | del(.commit) 
    else .["is_stable"]=false 
    end |
    del(.book_list)] | sort_by(.sortkey)' <<<"${__cst_config_data}")"

declare -i i=0
declare -i len=$(jq 'length' <<<"$array")
while [[ $i -lt $len ]]; do
    version_data=$(jq ".[$i]" <<<"$array")
    source_repo="$(jq --raw-output '.source_repo' <<<"$version_data")"
    main_repo_history_url="$(jq --raw-output '.main_repo_history_url' <<<"${__cst_config_data}")"
    source_repo_commit="$(cd ${__cst_repo_path}/xml/$source_repo; git rev-parse HEAD;)"
    source_repo_commit_short=${source_repo_commit:0:7}
    if [[ $(jq --raw-output ".is_stable" <<<"$version_data") == "false" ]]; then
        xml_file="${__cst_repo_path}/${__cst_xml_results_dir}/$source_repo/edition/cst-edition_page-view.xml"
        timestamp="$(xmlstarlet sel \
            -N tei="http://www.tei-c.org/ns/1.0" \
            -t -v "/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:p/tei:date/text()" \
            "$xml_file")"
        build_date="$(date --date=$timestamp +%d.%m.%Y)"    
        build_time="$(date --date=$timestamp +%H:%M)"
        main_repo_commit="$(cd ${__cst_repo_path}; git rev-parse HEAD)"
        source_repo_history_url="$(jq --raw-output '.source_repo_history_url' <<<"$version_data")"
    else
        current_path=$(pwd)
        cd ${__cst_repo_path}/xml/$source_repo
        build_date="$(git show -s --format=%cd --date=format:%d.%m.%Y $source_repo_commit)"
        build_time="$(git show -s --format=%cd --date=format:%H:%M $source_repo_commit)"
        cd $current_path
        main_repo_commit=$(jq --raw-output ".main_repo_commit" <<<"$version_data")
        source_repo_history_url="$(jq --raw-output ".builds[] | select(.source_repo==\"$source_repo\") | .source_repo_history_url" <<<"${__cst_config_data}")"
    fi
    main_repo_commit_short=${main_repo_commit:0:7}
    array="$(jq \
        --argjson _i $i \
        --arg _build_date $build_date \
        --arg _build_time $build_time \
        --arg _main_repo_commit $main_repo_commit \
        --arg _main_repo_commit_short $main_repo_commit_short \
        --arg _main_repo_history_url $main_repo_history_url \
        --arg _source_repo_commit $source_repo_commit \
        --arg _source_repo_commit_short $source_repo_commit_short \
        'if .[$_i].main_repo_commit? then .
        else .[$_i] += {"main_repo_commit": $_main_repo_commit}
        end |
        .[$_i] += {"build_date": $_build_date} |
        .[$_i] += {"build_time": $_build_time} |
        .[$_i] += {"main_repo_commit_short": $_main_repo_commit_short} |
        .[$_i] += {"main_repo_history_url": $_main_repo_history_url} |
        .[$_i] += {"source_repo_commit": $_source_repo_commit} |
        .[$_i] += {"source_repo_commit_short": $_source_repo_commit_short}' <<<"$array")"
    ((i+=1))
done

# Write to a temporary file
tmp_versions_index_json="$(mktemp)"
jq -n --argjson _array "$array" '{"versions": $_array }' >"$tmp_versions_index_json"

# Update versions_index.json only if content changes
if ! cmp -s "$tmp_versions_index_json" "versions_index.json"; then
    mv "$tmp_versions_index_json" "versions_index.json"
else
    rm "$tmp_versions_index_json"
fi
