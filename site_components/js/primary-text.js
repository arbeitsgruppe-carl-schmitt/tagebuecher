"use strict"

function Frit(parameters) {
	const fritViewer = document.querySelector('.frit-viewer');
	const mainContent = document.querySelector('main');

	/********************************
	*	Font Size Functionality		*
	********************************/

	const fontSizeButtons = document.querySelectorAll('input[name="frit-font-size-button"]');
	let fontSizeDesc = editionPage.getFontSizeDesc();
	for (const button of fontSizeButtons) {
		if (button.value === fontSizeDesc) {
			button.setAttribute('checked', '');
			break;
		}
	}

	const handleClickOnFontSizeMenu = event => {
		if (!event.target.closest('#frit-font-size-menu')) {
			return;
		}
		if (event.target.closest('#frit-large-font-button')) {
			editionPage.setFontSize('large');
		}
		else if (event.target.closest('#frit-medium-font-button')) {
			editionPage.setFontSize('medium');
		}
		else if (event.target.closest('#frit-small-font-button')) {
			editionPage.setFontSize('small');
		}
	}

	/****************************
	*	Textmode Functionality	*
	****************************/

	let textMode = localStorage.getItem('textMode');
	if (textMode === null) {
		localStorage.setItem('textMode', 'standard-text-mode');
		textMode = 'standard-text-mode';
	}
	fritViewer.classList.add(textMode);
	const textModeButtons = document.querySelectorAll('input[name="text-mode-button"]');
	for (const button of textModeButtons) {
		if (button.value === textMode) {
			button.checked = true;
			break;
		}
	}

	const setTextMode = () => {
		let newTextMode;
		for (const button of textModeButtons) {
			if (button.checked) {
				newTextMode = button.value;
				break;
			}
		}

		if (newTextMode !== textMode) {
			fritViewer.classList.remove(textMode);
			fritViewer.classList.add(newTextMode);
			localStorage.setItem('textMode', newTextMode);
			textMode = newTextMode;
		}
	};

	const handleClickOnTextModeMenu = event => {
		if (!event.target.closest('#text-mode-menu')) {
			return;
		}
		setTextMode();
	}

	/****************************
	*	Theme Functionality		*
	****************************/

	const theme_menu_buttons = document.querySelectorAll('input[name="frit-color-theme-button"]');
	let activeTheme = localStorage.getItem('theme');
	for (const button of theme_menu_buttons) {
		if (button.value === activeTheme) {
			button.setAttribute('checked', '');
			break;
		}
	}
	const handleClickOnThemeMenu = event => {
		if (!event.target.closest('#theme-menu')) {
			return;
		}
		let newThemeName;
		for (const button of theme_menu_buttons) {
			if (button.checked) {
				newThemeName = button.value;
				if (newThemeName !== localStorage.getItem('theme')) {
					editionPage.setTheme(newThemeName);
				}
				break;
			}
		}
	}

	window.addEventListener('click', event => {
		handleClickOnFontSizeMenu(event)
		handleClickOnTextModeMenu(event);
		handleClickOnThemeMenu(event);
	});
}