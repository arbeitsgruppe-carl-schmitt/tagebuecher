"use strict"

function EditionPage(parameters) {
    const pageName = parameters.name;
    this.getPageName = function() {
        return pageName
    }
    const rootEl = document.querySelector('html');

    /*********************************
    *   Switch Theme Functionality   *
    *********************************/

    this.setTheme = function(themeName) {
        localStorage.setItem('theme', themeName);
        document.documentElement.className = themeName;
        updateSwitchThemeButton(themeName);
    }

    const switchThemeButton = document.querySelector('#switch-theme-button');

    const toggleTheme = () => {
        if (localStorage.getItem('theme') === 'dark-theme') {
            this.setTheme('black-theme');
        }
        else if (localStorage.getItem('theme') === 'black-theme') {
            this.setTheme('bright-theme');
        }            
        else if (localStorage.getItem('theme') === 'bright-theme') {
            this.setTheme('light-theme');
        }
        else if (localStorage.getItem('theme') === 'light-theme') {
            this.setTheme('dark-theme');
        }
    }
    const updateSwitchThemeButton = themeName => {
        if (themeName === 'bright-theme') {
            switchThemeButton.innerHTML = '<i class="fa-duotone fa-cloud-sun"></i>';
            switchThemeButton.title = 'Weniger Kontraste. (Sanftes Farbschema)';
        }
        else if (themeName === 'light-theme') {
            switchThemeButton.innerHTML = '<i class="fa-duotone fa-moon-stars"></i>';
            switchThemeButton.title = 'Lichter herunterdimmen. (Dunkles Farbschema)';
        }
        else if (themeName === 'dark-theme') {
            switchThemeButton.innerHTML = '<i class="fa-duotone fa-clouds-moon"></i>';
            switchThemeButton.title = 'Licht aus! (Schwarzes Farbschema)';
        }
        else if (themeName === 'black-theme') {
            switchThemeButton.innerHTML = '<i class="fa-duotone fa-sun"></i>';
            switchThemeButton.title = 'Licht an! (Helles Farbschema)';
        }
    }
    (() => {
        if (localStorage.getItem('theme') === 'dark-theme') {
            this.setTheme('dark-theme');
        }
        else if (localStorage.getItem('theme') === 'black-theme') {
            this.setTheme('black-theme');
        }
        else if (localStorage.getItem('theme') === 'bright-theme') {
            this.setTheme('bright-theme');
        }
        else {
            this.setTheme('light-theme');
        }
    })();

    /*************************************
    *   Accordion Button Functionality   *
    *************************************/

    const handleClickOnAccordionButton = event => {
        if (!event.target.closest('.accordion')) {
            return;
        }
        let accordion = event.target.closest('.accordion-wrapper');

        let button = event.target.closest('.accordion');
        let panel = button.closest('.accordion-wrapper').querySelector('.panel');
        let icon = button.querySelector('i');
        let newIcon = document.createElement('i');
        newIcon.classList.add('fas');
        button.classList.toggle('active');
        if (panel.dataset.visible === 'true') {
            closeAccordion(accordion);
            /* panel.dataset.visible = 'false';
            panel.style.display = 'none';
            newIcon.classList.add('fa-angle-down');
            button.title='Inhalt anzeigen'; */
        } else if (panel.dataset.visible === 'false') {
            panel.dataset.visible = 'true';
            panel.style.display = 'block';
            newIcon.classList.add('fa-angle-up');
            button.title='Inhalt verbergen';
        }
        icon.replaceWith(newIcon);
    }

    const closeAccordion = accordion => {
        let button = accordion.querySelector('.accordion');
        let icon = button.querySelector('i');
        let panel = accordion.querySelector('.panel');
        let newIcon = document.createElement('i');
        newIcon.classList.add('fas', 'fa-angle-down');
        panel.dataset.visible = 'false';
        panel.style.display = 'none';
        button.title='Inhalt anzeigen';
        icon.parentNode.replaceChild(newIcon, icon);
    }

    /****************************************
    *   Click Dropdown Menu Functionality   *
    ****************************************/

    const clickDropdownMenus = document.querySelectorAll('.click.dropdown');
    const closeInactiveDropdownMenus = event => {
        for (let i=0; i<clickDropdownMenus.length; i++) {
            let dropContent = clickDropdownMenus[i].querySelector('.dropdown-content');
            if (clickDropdownMenus[i] !== event.target.closest('.click.dropdown')
                && dropContent.classList.contains('show-block-content')) {
                dropContent.classList.toggle('show-block-content');
        }
    }
}
const handleClickOnDropMenuButton = event => {
    closeInactiveDropdownMenus(event);
    if (!event.target.closest('.click.dropdown')) {
        return;
    }
    if (event.target.closest('.click.dropdown > button')) {
        let dropMenu = event.target.closest('.click.dropdown');
        let dropContent = dropMenu.querySelector('.dropdown-content');
        dropContent.classList.toggle('show-block-content');
    }
}

    /*****************************
    *  Font Size Functionality   *
    *****************************/

const largeFontSize = '18px';
const mediumFontSize = '16px';
const smallFontSize = '14px';
let fontSizeDesc;

let fontSize = localStorage.getItem('fontSize');
if (fontSize === null) {
    fontSize = mediumFontSize;
}
rootEl.style.fontSize = fontSize;
if (fontSize === largeFontSize) {
    fontSizeDesc = 'large';
}
else if (fontSize === mediumFontSize) {
    fontSizeDesc = 'medium';
}
else if (fontSize === smallFontSize) {
    fontSizeDesc = 'small';
}
this.getFontSizeDesc = function() {
    return fontSizeDesc;
}

this.setFontSize = function(newFontSizeDesc) {
    let newFontSizeVal;
    if (newFontSizeDesc === 'large' ) {
        newFontSizeVal = largeFontSize;
    }
    else if (newFontSizeDesc === 'medium') {
        newFontSizeVal = mediumFontSize;
    }
    else if (newFontSizeDesc === 'small') {
        newFontSizeVal = smallFontSize;
    }
    if (newFontSizeVal === fontSize) {
        return;
    }
    fontSizeDesc = newFontSizeDesc;
    fontSize = newFontSizeVal;
    rootEl.style.fontSize = fontSize;
    localStorage.setItem('fontSize', fontSize);
}


const handleClickOnFontSizeButton = event => {
    if (!event.target.closest('#font-size-buttons-wrapper')) {
        return;
    }
    if (event.target.closest('#large-font-size-button')) {
        this.setFontSize('large');
    }
    if (event.target.closest('#medium-font-size-button')) {
        this.setFontSize('medium');
    }
    if (event.target.closest('#small-font-size-button')) {
        this.setFontSize('small');
    }
}

    /**********************************************
    *   Main Nav Bar Theme Button Functionality   *
    **********************************************/

const handleClickOnThemeButton = event => {
    if (!event.target.closest('#switch-theme-button')) {
        return;
    }
    toggleTheme();
}

window.addEventListener('click', event => {
    handleClickOnAccordionButton(event),
    handleClickOnDropMenuButton(event),
    handleClickOnFontSizeButton(event),
    handleClickOnThemeButton(event);
});

}