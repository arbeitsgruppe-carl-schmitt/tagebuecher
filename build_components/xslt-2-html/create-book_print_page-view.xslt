<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    exclude-result-prefixes="cst entx gndo xs"
    version="3.0"
    xmlns:cst="https://carlschmitt.bio/entityxml/terms#"
    xmlns:entx="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:import href="basic-templates.xslt"/>
    <xsl:import href="low-level-xml-to-html-templates.xslt"/>
    <xsl:param name="book-id" select="book-id"/>
    <xsl:output indent="yes" method="html" version="5"/>
    
    <xsl:variable name="persons" as="element()*" select="document('../../xml/entities/persons.xml')/entx:entityXML/entx:collection/entx:data/entx:list/entx:person"/>
    
    <xsl:template match="/">
        <div class="editorial">
            <h1><xsl:value-of select="/teiCorpus/TEI[@xml:id = $book-id]/teiHeader/fileDesc/titleStmt/title[not(@*)]"/></h1>
            <h2>Editorischer Bericht</h2>
            <xsl:apply-templates select="/teiCorpus/TEI[@xml:id = $book-id]/teiHeader/fileDesc/sourceDesc"/>
            <xsl:variable name="german-date" select="format-dateTime(xs:dateTime(/teiCorpus/teiHeader/fileDesc/publicationStmt/p/date[1]), '[D01].[M01].[Y0001], [H01]:[m01]')"/>
            <div class="version-info">
                <h2>Versionsinformation</h2>
                <p>Entwicklungsversion 0.2<br/>
                    Quellrepositorium: "internal"<br/>
                    Letzte Änderung: <xsl:value-of select="$german-date"/><br/>
                    Commit des XML-Quellrepositoriums: [...]<br/>
                    Commit des Transformationsrepositoriums: [...]
                </p>
            </div>
        </div>
        <xsl:apply-templates select="/teiCorpus/TEI[@xml:id = $book-id]/text/body/div[@type = 'entry']"/>
    </xsl:template>
    
    <xsl:template match="choice[child::abbr]">
        <xsl:apply-templates select="abbr"/>
    </xsl:template>
    
    <xsl:template match="div[@type = 'entry' and not(contains(@xml:id, '_part-'))]">
        <div class="entry" data-type="atom">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="div[@type='entry'
        and contains(@xml:id, '_part-')]">
        <xsl:variable name="prev-part-id">
            <xsl:call-template name="get-prev-part-id"/>
        </xsl:variable>
        <xsl:variable name="next-part-id">
            <xsl:call-template name="get-next-part-id"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="preceding-sibling::*[1][local-name()='pb']
                and preceding-sibling::*[@type='entry'][1][@xml:id=$prev-part-id]"/>
            <xsl:when test="following-sibling::*[@type='entry'][1][@xml:id=$next-part-id]">
                <div class="entry" data-type="chain">
                    <xsl:apply-templates/>
                    <xsl:call-template name="get-next-part-of-chapter"/>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <div class="entry" data-type="fragment">
                    <xsl:apply-templates/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
        
    <xsl:template match="lb[@n and not(@type='first')]">
        <xsl:variable name="class-val">
            <xsl:choose>
                <xsl:when test="@type">
                    <xsl:value-of select="'lb ' || @type"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'lb standard'"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <span class="{$class-val}"><xsl:value-of select="'[' || @n || ']'"/></span>
    </xsl:template>
    
    <xsl:template match="note[parent::app]">
        <span class="edit-fn">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
    <xsl:template match="rs[@type='person' and starts-with(@ref, '#__GND_')]" priority="1">
        <span class="rs {@type}">
            <xsl:variable name="uri" select="'https://d-nb.info/gnd/' || substring-after(@ref, '#__GND_')"/>
            <xsl:choose>
                <xsl:when test="$persons[@gndo:uri=$uri]">
                    <xsl:variable name="p" as="element()" select="$persons[@gndo:uri=$uri]"/>
                    <xsl:variable name="name">
                        <xsl:choose>
                            <xsl:when test="$p/gndo:preferredName/gndo:forename and $p/gndo:preferredName/gndo:surname">
                                <xsl:value-of select="$p/gndo:preferredName/gndo:forename || ' ' || $p/gndo:preferredName/gndo:surname"/>
                            </xsl:when>
                            <xsl:when test="$p/gndo:preferredName/gndo:personalName">
                                <xsl:value-of select="$p/gndo:preferredName/gndo:personalName"/>
                            </xsl:when>
                            <xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:variable name="lifespan">
                        <xsl:choose>
                            <xsl:when test="matches($p/gndo:dateOfBirth/@iso-date, '^[0-9]{4}')
                                and matches($p/gndo:dateOfDeath/@iso-date, '^[0-9]{4}')">
                                <xsl:value-of select="' (' || substring($p/gndo:dateOfBirth/@iso-date,1,4) || '-' || substring($p/gndo:dateOfDeath/@iso-date,1,4) || ')'"/>
                            </xsl:when>
                            <xsl:otherwise/>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:apply-templates/>
                    <span class="entity-note"><xsl:value-of select="$name || $lifespan"/></span>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
        </span>
    </xsl:template>
    <xsl:template match="rs[@type='work'
        or (@type='person' and matches(@ref, '^#[A-Za-z]'))]"  priority="1">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="text//ab">
        <xsl:choose>
            <xsl:when test="ends-with(@xml:id, '_part-2')"/>
            <xsl:when test="ends-with(@xml:id, '_part-1')
                and preceding-sibling::*[local-name()='ab']">
                <p><span class="indent"/><xsl:apply-templates/><xsl:call-template name="get-next-ab-part"/></p>
            </xsl:when>
            <xsl:when test="ends-with(@xml:id, '_part-1') and not(preceding-sibling::*[local-name()='ab'])">
                <p><xsl:call-template name="check-if-pb"/><xsl:apply-templates/><xsl:call-template name="get-next-ab-part"/></p>
            </xsl:when>
            <xsl:when test="preceding-sibling::*[local-name()='ab']">
                <p><span class="indent"/><xsl:apply-templates/></p>
            </xsl:when>
            <xsl:when test="(parent::div[@type='entry'
                and contains(@xml:id, '_part-')
                and not(ends-with(@xml:id, '_part-1'))
                and preceding-sibling::*[@type='entry'][1][contains(@xml:id, '_part-')]])">
                <xsl:variable name="preceding-part-id" select="parent::div[@type='entry']/preceding-sibling::div[@type='entry'][1]/@xml:id"/>
                <xsl:variable name="prev-part-id">
                    <xsl:for-each select="parent::div[@type='entry']">
                        <xsl:call-template name="get-prev-part-id"/>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$preceding-part-id=$prev-part-id">
                        <p><span class="indent"/><xsl:call-template name="check-if-pb"/><xsl:apply-templates/></p>
                    </xsl:when>
                    <xsl:otherwise>
                        <p><xsl:call-template name="check-if-pb"/><xsl:apply-templates/></p>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="not(preceding-sibling::*[local-name()='ab'])">
                <p><xsl:call-template name="check-if-pb"/><xsl:apply-templates/></p>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="get-next-ab-part">
        <xsl:variable name="next-id">
            <xsl:call-template name="get-next-part-id"/>
        </xsl:variable>
        <xsl:if test="ancestor::div[@type='entry'][1]/following-sibling::*[@type='entry'][1]/ab[@xml:id=$next-id]">
            <xsl:for-each select="ancestor::div[@type='entry'][1]/following-sibling::*[@type='entry'][1]/ab[@xml:id=$next-id][1]">
                <xsl:call-template name="check-if-pb"/>
                <xsl:apply-templates select="./node()"/>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="get-next-part-of-chapter">
        <xsl:variable name="next-id">
            <xsl:call-template name="get-next-part-id"/>
        </xsl:variable>
        <xsl:if test="following-sibling::*[@type='entry'][1][@xml:id=$next-id]">
            <xsl:for-each select="following-sibling::*[@type='entry'][1][@xml:id=$next-id]">
                <xsl:apply-templates select="node()"/>
                <xsl:if test="following-sibling::*[1][local-name()='pb']">
                    <xsl:call-template name="get-next-part-of-chapter"/>
                </xsl:if>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="get-next-part-id">
        <xsl:variable name="n" select="number(substring-after(@xml:id, '_part-'))+1"/>
        <xsl:value-of select="substring-before(@xml:id, '_part-') || '_part-' || format-number($n, '#')"/>
    </xsl:template>
    <xsl:template name="get-prev-part-id">
        <xsl:variable name="n" select="number(substring-after(@xml:id, '_part-'))-1"/>
        <xsl:value-of select="substring-before(@xml:id, '_part-') || '_part-' || format-number($n, '#')"/>
    </xsl:template>
    
    <xsl:template name="check-if-pb">
        <xsl:if test="parent::div[@type = 'entry' and preceding-sibling::*[1][local-name() = 'pb']]">
            <xsl:variable name="pb-count" select="preceding::milestone[@unit = 'image'][1]/cst:erase-zeros(tokenize(@xml:id, '_')[last()])"/>
            <xsl:variable name="pb-label" select="preceding::pb[1]/@n"/>
            <xsl:variable name="pb-orientation">
                <xsl:choose>
                    <xsl:when test="$pb-label = 'left-page'">
                        <xsl:value-of select="'l'"/>
                    </xsl:when>
                    <xsl:when test="$pb-label = 'right-page'">
                        <xsl:value-of select="'r'"/>
                    </xsl:when>
                    <xsl:when test="$pb-label = 'recto'">
                        <xsl:value-of select="'recto'"/>
                    </xsl:when>
                    <xsl:when test="$pb-label = 'verso'">
                        <xsl:value-of select="'verso'"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:variable>
            <span class="orig-pb-call">|</span><span class="orig-pb-content"><span class="orig-pb-marker">|</span><xsl:value-of select="$pb-count || $pb-orientation"/></span>
        </xsl:if>
    </xsl:template>
        
    <xsl:template name="get-next-part">
        <xsl:variable name="n" select="number(substring-after(@xml:id, '_part-'))+1"/>
        <xsl:variable name="next-part-id" select="substring-before(@xml:id, '_part-') || '_part-' || format-number($n, '#')"/>
        <xsl:if test="/TEI//text//*[@xml:id=$next-part-id]">
            <xsl:for-each select="/TEI//text//*[@xml:id=$next-part-id][1]">
                <xsl:apply-templates select="node()"/>
                <xsl:call-template name="get-next-part"/>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
