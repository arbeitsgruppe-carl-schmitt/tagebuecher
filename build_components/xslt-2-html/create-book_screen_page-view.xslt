<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="cst xs" version="3.0"
   xmlns:cst="https://carlschmitt.bio/entityxml/terms#"
   xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xpath-default-namespace="http://www.tei-c.org/ns/1.0">
   <xsl:import href="basic-templates.xslt"/>
   <xsl:import href="low-level-xml-to-html-templates.xslt"/>
   <xsl:output indent="yes" method="html" version="5"/>
   <xsl:param name="book-id" select="book-id"/>
   <xsl:variable name="to-iiif-scan-text" select="'Scan dieses Bildes öffnen'"/>
   
   <xsl:template match="/">
      <xsl:apply-templates select="/teiCorpus/TEI[@xml:id = $book-id]/text/front"/>
      <xsl:apply-templates select="/teiCorpus/TEI[@xml:id = $book-id]/text/body/milestone[@unit = 'image' and @xml:id]"/>
   </xsl:template>
   
   <xsl:template match="ab"><p><xsl:if test="@type">
      <xsl:attribute name="class" select="@type"/>
   </xsl:if><xsl:apply-templates/></p>
   </xsl:template>
   
   <xsl:template match="app[child::note and not(parent::quote)]">
      <span class="editorial-comment" title="{./note//text()}">{{ icons.asterisk }}</span>
   </xsl:template>
   
   <xsl:template match="choice[child::abbr]">
      <xsl:apply-templates select="abbr"/>
   </xsl:template>
   
   <xsl:template match="front">
      <div class="frit front">
         <div class="frit-description">
            <xsl:call-template name="create-image-transcription-link"/>
            <xsl:call-template name="create-scan-link">
               <xsl:with-param name="description" select="'Vorderer Umschlag'"/>
            </xsl:call-template>
         </div>
         <div class="spread-wrapper">
            <div class="page left-page data-empty"/>
            <div class="page right-page">
               <xsl:apply-templates select="head"/>
            </div>
         </div>
      </div>
   </xsl:template>
   
   <xsl:template match="front/head">
      <header>
         <xsl:if test="@hand">
            <xsl:attribute name="class" select="@hand"/>
         </xsl:if>
         <xsl:apply-templates/>
      </header>
   </xsl:template>
   
   <xsl:template match="lb">
      <xsl:if test="@type = 'hard'">
         <br/>
      </xsl:if>
      <span class="lb">
         <xsl:choose>
            <xsl:when test="@type = 'first'">
               <xsl:attribute name="data-first"/>
            </xsl:when>
            <xsl:when test="@type = 'hard'">
               <xsl:attribute name="data-hard"/>
            </xsl:when>
            <xsl:when test="@type = 'in-word'">
               <xsl:attribute name="data-in-word"/>
            </xsl:when>
         </xsl:choose>
         <xsl:if test="@n">
            <span class="lbCountBracket">[</span>
            <xsl:value-of select="@n"/>
            <span class="lbCountBracket">]</span>
         </xsl:if>
      </span>
   </xsl:template>
   
   <xsl:template match="milestone[@unit = 'image']">
      <xsl:variable name="imageTextContent">
         <xsl:call-template name="getImageTextContent"/>
      </xsl:variable>
      <div class="frit {@type}">
         <div class="frit-description">
            <xsl:call-template name="create-image-transcription-link"/>
            <xsl:variable name="description">
               <xsl:choose>
                  <xsl:when test="@type = 'inlaid-sheet'">
                     <xsl:value-of select="'Eingelegter Zettel'"/>
                  </xsl:when>
                  <xsl:when test="$imageTextContent/div[@subtype = 'excerpt']">
                     <xsl:value-of select="'Parallelisierte Ansicht von Exzerpten'"/>
                  </xsl:when>
                  <xsl:when test="@type = 'open-book'">
                     <xsl:value-of select="'Aufgeschlagenes Heft'"/>
                  </xsl:when>
               </xsl:choose>
            </xsl:variable>
            <xsl:call-template name="create-scan-link">
               <xsl:with-param name="description" select="$description"/>
            </xsl:call-template>
         </div>
         
         <div class="spread-wrapper">
            <xsl:choose>
               <xsl:when test="$imageTextContent/div[@subtype = 'excerpt']">
                  <xsl:call-template name="create-linearized-spread">
                     <xsl:with-param name="imageTextContent" select="$imageTextContent"/>
                  </xsl:call-template>
               </xsl:when>
               <xsl:when test="
                  $imageTextContent/div[@subtype = 'diary' or @subtype = 'paratext']">
                  <xsl:call-template name="create-material-spread">
                     <xsl:with-param name="imageTextContent" select="$imageTextContent"/>
                  </xsl:call-template>
               </xsl:when>
            </xsl:choose>
         </div>
      </div>
   </xsl:template>
   
   <xsl:template name="create-linearized-spread">
      <xsl:param name="imageTextContent"/>
      <xsl:variable name="leftPageContent">
         <xsl:apply-templates select="$imageTextContent/div[@subtype = 'excerpt']"/>
      </xsl:variable>
      <xsl:variable name="rightPageContent">
         <xsl:apply-templates select="$imageTextContent/div[@subtype = 'excerpt']/note[@target]"/>
      </xsl:variable>
      <xsl:call-template name="createPage">
         <xsl:with-param name="pageContent" select="$leftPageContent"/>
         <xsl:with-param name="thisPageClass" select="'excerpt-col'"/>
         <xsl:with-param name="pageLabel" select="'Haupttext'"/>
      </xsl:call-template>
      <xsl:call-template name="createPage">
         <xsl:with-param name="pageContent" select="$rightPageContent"/>
         <xsl:with-param name="thisPageClass" select="'annotation-col'"/>
         <xsl:with-param name="pageLabel" select="'Annotationen'"/>
      </xsl:call-template>
   </xsl:template>
   
   <xsl:template name="create-image-transcription-link">
      <span class="image-transcription-link" id="{@xml:id}"
         title="URL dieses Fragments in Zwischenablage kopieren">{{ icons.link }}Bild <xsl:value-of select="cst:erase-zeros(tokenize(@xml:id, '_')[last()])"
         /></span>
   </xsl:template>
   
   <xsl:template name="create-material-spread">
      <xsl:param name="imageTextContent"/>
      <xsl:variable name="leftPageClass">
         <xsl:choose>
            <xsl:when test="@type = 'inlaid-sheet'">
               <xsl:value-of select="'verso'"/>
            </xsl:when>
            <xsl:when test="@type = 'open-book'">
               <xsl:value-of select="'left-page'"/>
            </xsl:when>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="rightPageClass">
         <xsl:choose>
            <xsl:when test="@type = 'inlaid-sheet'">
               <xsl:value-of select="'recto'"/>
            </xsl:when>
            <xsl:when test="@type = 'open-book'">
               <xsl:value-of select="'right-page'"/>
            </xsl:when>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="leftPageLabel">
         <xsl:choose>
            <xsl:when test="@type = 'inlaid-sheet'">
               <xsl:value-of select="'Rückseite'"/>
            </xsl:when>
            <xsl:when test="@type = 'open-book'">
               <xsl:value-of select="'linke Seite'"/>
            </xsl:when>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="rightPageLabel">
         <xsl:choose>
            <xsl:when test="@type = 'inlaid-sheet'">
               <xsl:value-of select="'Vorderseite'"/>
            </xsl:when>
            <xsl:when test="@type = 'open-book'">
               <xsl:value-of select="'rechte Seite'"/>
            </xsl:when>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="leftPageContent">
         <xsl:apply-templates
            select="$imageTextContent/pb[@n = $leftPageClass]/following-sibling::element()[not(preceding-sibling::pb[@n = $rightPageClass])]"
         />
      </xsl:variable>
      <xsl:variable name="rightPageContent">
         <xsl:apply-templates
            select="$imageTextContent/pb[@n = $rightPageClass]/following-sibling::element()"/>
      </xsl:variable>
      <xsl:call-template name="createPage">
         <xsl:with-param name="pageContent" select="$leftPageContent"/>
         <xsl:with-param name="thisPageClass" select="$leftPageClass"/>
         <xsl:with-param name="pageLabel" select="$leftPageLabel"/>
         
      </xsl:call-template>
      <xsl:call-template name="createPage">
         <xsl:with-param name="pageContent" select="$rightPageContent"/>
         <xsl:with-param name="thisPageClass" select="$rightPageClass"/>
         <xsl:with-param name="pageLabel" select="$rightPageLabel"/>
      </xsl:call-template>
   </xsl:template>
   
   <xsl:template name="createPage">
      <xsl:param name="thisPageClass"/>
      <xsl:param name="pageLabel"/>
      <xsl:param name="pageContent"/>
      <div class="page {$thisPageClass}">
         <xsl:if test="not($pageContent//text()[matches(., '[A-Za-z0-9]')])">
            <xsl:attribute name="data-empty"/>
         </xsl:if>
         <div class="page-info">
            <span class="page-label">
               <xsl:value-of select="$pageLabel"/>
            </span>
         </div>
         <div class="page-content-wrapper">
            <div class="anchor-col"/>
            <div class="page-content">
               <xsl:copy-of select="$pageContent"/>
            </div>
         </div>
      </div>
   </xsl:template>
   
   <xsl:template match="div[@subtype = 'excerpt']">
      <div class="excerpt">
         <xsl:apply-templates select="*[not(local-name() = 'note' and @target)]"/>
      </div>
   </xsl:template>
   
   <xsl:template match="head[ancestor::div[@subtype = 'excerpt']]">
      <h1>
         <xsl:apply-templates/>
      </h1>
   </xsl:template>
   
   <xsl:template match="seg[@type = 'call-number']">
      <span class="call-number">
         <xsl:apply-templates/>
      </span>
   </xsl:template>
   
   <xsl:template match="seg[@rend = 'longhand']">
      <span class="longhand">
         <xsl:apply-templates/>
      </span>
   </xsl:template>
   
   <xsl:template match="seg[@type = 'signature-group']">
      <span class="signature-group">
         <xsl:apply-templates/>
      </span>
   </xsl:template>
   
   <!-- Named templates -->
   
   <xsl:template name="create-scan-link">
      <xsl:param name="description"/>
      <xsl:value-of select="'('"/>
      <xsl:element name="a">
         <xsl:attribute name="class" select="'link-to-iiif-scan'"/>
         <xsl:attribute name="href">
            <xsl:choose>
               <xsl:when test="@facs">
                  <xsl:value-of select="@facs"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="'#'"/>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:attribute>
         <xsl:if test="@facs">
            <xsl:attribute name="target" select="'_blank'"/>
         </xsl:if>
         <xsl:attribute name="title" select="$to-iiif-scan-text"/>
         <xsl:value-of select="'{{ icons.image }}' || $description"/>
      </xsl:element>
      <xsl:value-of select="')'"/>
   </xsl:template>
   
</xsl:stylesheet>
