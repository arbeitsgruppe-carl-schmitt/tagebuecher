<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="cst xs" version="3.0"
    xmlns:cst="https://carl-schmitt-tagebuecher.de/cst-ns"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:import href="low-level-xml-to-html-templates.xslt"/>
    <xsl:output indent="no" method="html" version="5"/>
    <xsl:template match="/">
        <h2>
            <xsl:value-of select="/TEI/teiHeader/fileDesc/titleStmt/title"/>
        </h2>
        <div class="accordion-wrapper">
            <button class="accordion bare" title="Inhalt verbergen">
                <h3>Editorischer Bericht {{ icons.angle_up }}</h3>
            </button>
            <div class="panel" data-visible="true">
                <xsl:apply-templates select="/TEI/teiHeader/fileDesc/sourceDesc/p"/>
            </div>
        </div>
    </xsl:template>


</xsl:stylesheet>
