<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="cst entx gndo xs" version="3.0" xmlns:cst="https://carlschmitt.bio/entityxml/terms#" xmlns:entx="https://sub.uni-goettingen.de/met/standards/entity-xml#" xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#">
   
   <xsl:output indent="yes" method="html"/>
   
   <xsl:template match="/">
      <div class="editorial-text">
         <div class="intro">
            <h1>Personenverzeichnis</h1>
            <p>Dieses maschinell erstellte <u>und zunächst noch vorläufige</u> Personenverzeichnis listet in alphabetischer Reihenfolge die Personen auf, die Carl Schmitt in seinen Tagebüchern nennt. Es enthält zusätzlich auch Personen, die lediglich als Autoren vorkommen. Die Lebensdaten und Informationen zur Disambiguierung stammen aus der <a href="https://www.dnb.de/DE/Professionell/Standardisierung/GND/gnd_node.html">Gemeinsamen Normdatei</a> der Deutschen Nationalbibliothek (GND). Zu einzelnen Personen ergänzen Informationen, die in Zusammenhang mit Carl Schmitt besonders erwähnenswert sind.</p>
            <p>Einträge mit dem Kürzel „<span style="font-family: var(--deco-font)">GND</span>“ in der Namenszeile sind bereits mit der GND verknüpft.</p>
            <p>Das Verzeichnis enthält aktuell noch zahlreiche Personen, die nicht oder unzureichend identifiziert wurden. Der Name "Juan Perez" wird als Platzhalter verwendet, wo derzeit keinerlei Informationen über eine Person angegeben werden können.</p>
            <p>Das Verzeichnis umfasst <xsl:value-of select="count(/entx:entityXML/entx:collection/entx:data/entx:list/entx:person)"/> Einträge.</p>
         </div>
      </div>
      <div class="frit-viewer">
         <nav>
         <xsl:for-each select="/entx:entityXML/entx:collection/entx:data/entx:list/entx:person">
            <xsl:sort select="cst:sortkey/text()"/>
            <div class="accordion-wrapper" data-sortkey="{cst:sortkey}">
               <h2 class="accordion entity-name" id="{@xml:id}"><button class="bare"><xsl:call-template name="get-person-name"/><xsl:call-template name="get-person-lifespan"/>{{ icons.angle_up }}<xsl:call-template name="create-gnd-mark"/></button></h2>
               <div class="panel" data-visible="true">
                  <xsl:if test="cst:info/text()">
                     <p>Bezug zu Carl Schmitt: <xsl:value-of select="cst:info/text()"/></p>
                  </xsl:if>
                  <xsl:if test="not(@gndo:uri)">
                     <p class="cst-id"><xsl:value-of select="'Interne ID: ' || @xml:id"/></p>
                  </xsl:if>
                  <xsl:if test="@gndo:uri or gndo:biographicalOrHistoricalInformation">
                     <h3>GND-Daten</h3>
                     <xsl:if test="gndo:biographicalOrHistoricalInformation">
                        <p class="gnd-bio"><xsl:value-of select="'Biografische Information: ' || gndo:biographicalOrHistoricalInformation[1]"/></p>
                     </xsl:if>
                     <xsl:if test="@gndo:uri">
                        <p class="gnd-id"><xsl:value-of select="'ID: '"/>
                           <a href="https://d-nb.info/gnd/{substring-after(@gndo:uri , 'https://d-nb.info/gnd/')}">
                              <xsl:value-of select="substring-after(@gndo:uri, 'https://d-nb.info/gnd/')"/>
                           </a></p>
                     </xsl:if>
                  </xsl:if>
               </div>
            </div>
         </xsl:for-each>
         </nav>
      </div>
   </xsl:template>
   
   <xsl:template name="create-gnd-mark">
      <xsl:if test="@gndo:uri">
         <span class="gnd-mark">GND</span>
      </xsl:if>
   </xsl:template>
   
   <xsl:template name="get-person-name">
      <xsl:choose>
         <xsl:when test="gndo:preferredName/gndo:surname/text() and gndo:preferredName/gndo:forename/text()">
            <xsl:value-of select="gndo:preferredName/gndo:surname/text() || ', ' || gndo:preferredName/gndo:forename/text()"/>
         </xsl:when>
         <xsl:when test="gndo:preferredName/gndo:surname/text()">
            <xsl:value-of select="gndo:preferredName/gndo:surname/text()"/>
         </xsl:when>
         <xsl:when test="gndo:preferredName/gndo:forename/text()">
            <xsl:value-of select="gndo:preferredName/gndo:forename/text()"/>
         </xsl:when>
         <xsl:when test="gndo:preferredName/gndo:personalName">
            <xsl:value-of select="gndo:preferredName/gndo:personalName"/>
         </xsl:when>
      </xsl:choose>
   </xsl:template>
   
   <xsl:template name="get-person-lifespan">
      <xsl:choose>
         <xsl:when test="gndo:dateOfBirth/@iso-date and gndo:dateOfDeath/@iso-date">
            <xsl:value-of select="' (' || tokenize(gndo:dateOfBirth/@iso-date, '-')[1] || '-' || tokenize(gndo:dateOfDeath/@iso-date, '-')[1] || ')'"/>
         </xsl:when>
         <xsl:when test="gndo:dateOfBirth/@iso-date">
            <xsl:value-of select="' (' || tokenize(gndo:dateOfBirth/@iso-date, '-')[1] || '-?)'"/>
         </xsl:when>
         <xsl:when test="gndo:dateOfDeath/@iso-date">
            <xsl:value-of select="' (?-' || tokenize(gndo:dateOfDeath/@iso-date, '-')[1] || ')'"/>
         </xsl:when>
      </xsl:choose>
   </xsl:template>
</xsl:stylesheet>
