<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="tei xs"
    version="3.0"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output method="text"/>
    <xsl:import href="low-level-xml-to-html-templates.xslt"/>
    <xsl:template match="/">
        <xsl:variable name="map" as="node()">
        <map xmlns="http://www.w3.org/2005/xpath-functions">
            <xsl:apply-templates select="/teiCorpus/TEI/text/body//app/note" mode="extract-notes">
                <xsl:sort select="@xml:id" order="ascending"/>
            </xsl:apply-templates>
        </map>
        </xsl:variable>
        <xsl:value-of select="xml-to-json($map)"/>
    </xsl:template>
    
    <xsl:template match="author | bibl | title" mode="extract-notes">
        <xsl:text>&lt;span class="</xsl:text><xsl:value-of select="local-name()"/><xsl:text>"></xsl:text><xsl:apply-templates mode="extract-notes"/><xsl:text>&lt;/span></xsl:text>
    </xsl:template>
    
    <xsl:template match="app/note" mode="extract-notes">
        <xsl:variable name="html-content">
            <xsl:apply-templates select="text() | bibl | q | quote | rs" mode="extract-notes"/>
        </xsl:variable>
        <string xmlns="http://www.w3.org/2005/xpath-functions" key="{@xml:id}">
            <xsl:value-of select="normalize-space($html-content)"/>
        </string>
    </xsl:template>
    
    <xsl:template match="q | quote" mode="extract-notes">
        <xsl:text>&lt;span class="</xsl:text><xsl:value-of select="local-name()"/><xsl:text>"></xsl:text>
        <xsl:value-of select="$double-quote-open"/>
        <xsl:apply-templates mode="extract-notes"/>
        <xsl:value-of select="$double-quote-close"/>
        <xsl:text>&lt;/span></xsl:text>
    </xsl:template>
    
    <xsl:template match="rs[@ref and
        (@type='person'
        or @type='place'
        or @type='work')]" mode="extract-notes">
        <xsl:text>&lt;a class="</xsl:text><xsl:value-of select="@type"/><xsl:text>"></xsl:text><xsl:apply-templates select="text()"/><xsl:text>{{ icons.long_arrow_up | replace: '"', '\"'}}&lt;/a></xsl:text>
    </xsl:template>
    
</xsl:stylesheet>
