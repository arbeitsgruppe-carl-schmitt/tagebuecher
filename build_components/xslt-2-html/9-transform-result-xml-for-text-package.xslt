<xsl:stylesheet version="3.0" xmlns="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:mode on-no-match="deep-copy"/>
    <xsl:template match="/">
        <xsl:apply-templates select="processing-instruction('xml-model')"/>
        <xsl:processing-instruction name="xml-stylesheet">href="cst-oxy-author-mode.css" type="text/css"</xsl:processing-instruction>
        <xsl:apply-templates select="/TEI | /teiCorpus"/>
    </xsl:template>    
</xsl:stylesheet>
