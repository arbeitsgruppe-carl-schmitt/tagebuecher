<xsl:stylesheet exclude-result-prefixes="cst xs" version="3.0" xmlns:cst="https://carl-schmitt-tagebuecher.de/cst-ns" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:variable name="double-quote-open" select="'„'"/>
    <xsl:variable name="double-quote-close" select="'“'"/>
    <xsl:variable name="single-quote-open" select="'‚'"/>
    <xsl:variable name="single-quote-close" select="'‘'"/>
    
    <xsl:template match="abbr">
        <xsl:choose>
            <xsl:when test="@rend">
                <span class="{@rend}"><xsl:apply-templates/></span>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="add">
        <span class="add"><xsl:apply-templates/></span>
    </xsl:template>
    
    <xsl:template match="app[parent::unclear and child::lem]">
        <span class="unclear"><xsl:apply-templates select="./lem/node()"/></span>
    </xsl:template>
    <xsl:template match="app/rdg"/>

    <xsl:template match="c[@type = 'ephemeral-hyphen']"/>
    <xsl:template match="c[@type = 'tab']">
        <span class="tab">
            <xsl:if test="@n">
                <xsl:attribute name="style" select="'width: ' || 0.42 * @n || 'rem;'"/>
            </xsl:if>
        </span>
    </xsl:template>
    
    <xsl:template match="date[@type = 'entry-date']">
        <span class="entry-date"><xsl:apply-templates/></span>
    </xsl:template>

    <xsl:template match="del">
        <span class="del"><xsl:apply-templates/></span>
    </xsl:template>

    <xsl:template match="head[ancestor::div[@subtype = 'excerpt']]">
        <h1><xsl:apply-templates/></h1>
    </xsl:template>
    
    <xsl:template match="hi">
        <span class="hi"><xsl:apply-templates/></span>
    </xsl:template>

    <xsl:template match="gap">
        <xsl:choose>
            <xsl:when test="@extent and matches(@extent, '[1-9]{1}$')">
                <xsl:call-template name="create-gap">
                    <xsl:with-param name="number" select="number(@extent)"/>
                    <xsl:with-param name="type" select="'word'"/>                    
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="@extent and matches(@extent, '[1-9]{1}[ ]{1}(letter[s]?|syllable[s]?|word[s]?)')">
                <xsl:call-template name="create-gap">
                    <xsl:with-param name="number" select="number(tokenize(@extent)[1])"/>
                    <xsl:with-param name="type" select="tokenize(@extent)[2]"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="create-gap">
                    <xsl:with-param name="number" select="1"/>
                    <xsl:with-param name="type" select="'word'"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="item[parent::list]">
        <li><xsl:apply-templates/></li>
    </xsl:template>
    
    <xsl:template match="l">
        <xsl:variable name="class-val">
            <xsl:call-template name="create-class-val">
                <xsl:with-param name="str" select="'l'"/>
            </xsl:call-template>
        </xsl:variable>
        <span class="{$class-val}">
            <xsl:if test="parent::lg[contains(@rend, 'quoted')]
                and not(preceding-sibling::l)">
                <xsl:variable name="quote-open-char">
                    <xsl:variable name="rend">
                        <xsl:choose>
                            <xsl:when test="contains(parent::lg/@rend, 'quoted-open-beginning')">
                                <xsl:value-of select="'quoted-open-beginning'"/>
                            </xsl:when>
                            <xsl:when test="contains(parent::lg/@rend, 'quoted')">
                                <xsl:value-of select="'quoted'"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="''"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:call-template name="get-quote-char-environment">
                        <xsl:with-param name="open-or-close" select="'open'"/>
                        <xsl:with-param name="rend" select="$rend"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:copy-of select="$quote-open-char"/>
            </xsl:if>
            <xsl:apply-templates/>
            <xsl:if test="parent::lg[contains(@rend, 'quoted')]
                and not(following-sibling::l)">
                <xsl:variable name="quote-close-char">
                    <xsl:variable name="rend">
                        <xsl:choose>
                            <xsl:when test="contains(parent::lg/@rend, 'quoted-open-end')">
                                <xsl:value-of select="'quoted-open-end'"/>
                            </xsl:when>
                            <xsl:when test="contains(parent::lg/@rend, 'quoted')">
                                <xsl:value-of select="'quoted'"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="''"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:call-template name="get-quote-char-environment">
                        <xsl:with-param name="open-or-close" select="'close'"/>
                        <xsl:with-param name="rend" select="$rend"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:copy-of select="$quote-close-char"/>
            </xsl:if>
        </span>
        <xsl:if test="parent::lg and following-sibling::l">
            <br/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="lg">
        <xsl:variable name="class-val">
            <xsl:call-template name="create-class-val">
                <xsl:with-param name="str" select="'lg'"/>
            </xsl:call-template>
        </xsl:variable>
        <p class="{$class-val}"><xsl:apply-templates/></p>
    </xsl:template>

    <xsl:template match="list">
        <ul><xsl:apply-templates/></ul>
    </xsl:template>

    <xsl:template match="note[parent::div]">
        <p class="note"><xsl:apply-templates/></p>
    </xsl:template>
    <xsl:template match="note[not(parent::div)]">
        <span class="note"><xsl:apply-templates/></span>
    </xsl:template>

    <xsl:template match="p">
        <p><xsl:apply-templates/></p>
    </xsl:template>
        
    <xsl:template match="q | quote">
        <xsl:variable name="class-val">
            <xsl:call-template name="create-class-val">
                <xsl:with-param name="str" select="local-name()"/>
            </xsl:call-template>
        </xsl:variable>
        <span class="{$class-val}"><xsl:call-template name="process-quoted-passage"/></span>
    </xsl:template>
    <xsl:template match="app[parent::quote]"/>
    
    <xsl:template match="rs[(@type = 'person' or @type = 'work') and starts-with(@ref, '#')]">
        <xsl:variable as="xs:boolean" name="is-first-entity">
            <xsl:call-template name="is-first-entity"/>
        </xsl:variable>
        <xsl:variable name="str">
            <xsl:choose>
                <xsl:when test="$is-first-entity">
                    <xsl:value-of select="@type"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="class-val">
            <xsl:call-template name="create-class-val">
                <xsl:with-param name="str" select="$str"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$is-first-entity">
                <a class="{$class-val}" href="../{@type}s/{substring-after(@ref, '#')}/" id="{@xml:id}"><xsl:call-template name="process-quoted-passage"/>{{ icons.long_arrow_up }}</a>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="string-length($class-val) > 0">
                        <span class="{$class-val}"><xsl:apply-templates/></span>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="seg[@rend = 'longhand']">
        <span class="longhand"><xsl:apply-templates/></span>
    </xsl:template>
    
    <xsl:template match="soCalled">
        <xsl:value-of select="$single-quote-open"/>
        <xsl:apply-templates/>
        <xsl:value-of select="$single-quote-close"/>
    </xsl:template>
    
    <xsl:template match="supplied">
        <span class="supplied-bracket">[</span>
        <xsl:apply-templates/>
        <span class="supplied-bracket">]</span>
    </xsl:template>

    <xsl:template match="unclear">
        <span class="unclear"><xsl:apply-templates/></span>
    </xsl:template>

    <xsl:template match="table">
        <table>
            <xsl:if test="@type">
                <xsl:attribute name="class" select="@type"/>
            </xsl:if>
            <thead>
                <xsl:for-each select="row[@style = 'head']">
                    <tr>
                        <xsl:for-each select="cell">
                            <th>
                                <xsl:value-of select="text()"/>
                            </th>
                        </xsl:for-each>
                    </tr>
                </xsl:for-each>
            </thead>
            <tbody>
                <xsl:for-each select="row[@style != 'head' or not(@style)]">
                    <tr>
                        <xsl:for-each select="cell">
                            <td>
                                <xsl:choose>
                                    <xsl:when test="text()
                                        and count(preceding-sibling::cell) = 2
                                        and not(matches(text(), ',[0-9]{2}$'))">
                                        <xsl:value-of select="text()"/><span style="visibility: hidden;">,00</span>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="text()"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                        </xsl:for-each>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table>
    </xsl:template>
    
    <!-- Named Templates Section -->
    
    <xsl:template name="create-class-val">
        <xsl:param name="str"/>
        <xsl:variable name="rend-val">
            <xsl:for-each select="tokenize(@rend, ' ')">
                <xsl:choose>
                    <xsl:when test=".='longhand'">
                        <xsl:value-of select="' longhand'"/>
                    </xsl:when>
                    <xsl:when test=".='del'">
                        <xsl:value-of select="' del'"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
        </xsl:variable>
        <xsl:value-of select="normalize-space($str || $rend-val)"/>
    </xsl:template>
    
    <xsl:template name="create-col-block">
        <xsl:apply-templates select="following-sibling::*[not(preceding-sibling::cb[@n='end'])]"/>
    </xsl:template>
    
    <xsl:template name="create-gap">
        <xsl:param name="extent"/>
        <xsl:param name="number"/>
        <xsl:param name="type"/>
        <xsl:variable name="class">
            <xsl:choose>
                <xsl:when test="starts-with($type, 'letter')">
                    <xsl:value-of select="'letter'"/>
                </xsl:when>
                <xsl:when test="starts-with($type, 'syllable')">
                    <xsl:value-of select="'syllable'"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'word'"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:if test="$number gt 0">
            <span class="gap {$class}"/>
        </xsl:if>
        <xsl:if test="$number gt 1">
            <xsl:call-template name="create-gap">
                <xsl:with-param name="number" select="$number - 1"/>
                <xsl:with-param name="type" select="$class"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template name="get-quote-char-environment">
        <xsl:param name="open-or-close"/>
        <xsl:param name="rend"/>
        <xsl:choose>
            <xsl:when test="starts-with($rend, 'quoted-open')">
                <span class="supplied-bracket"><xsl:value-of select="'['"/></span>
                <xsl:call-template name="get-quote-char">
                    <xsl:with-param name="position" select="$open-or-close"/>
                </xsl:call-template>
                <span class="supplied-bracket"><xsl:value-of select="']'"/></span>
            </xsl:when>
            <xsl:when test="$rend = 'quoted'">
                <xsl:call-template name="get-quote-char">
                    <xsl:with-param name="position" select="$open-or-close"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="process-quoted-passage">
        <xsl:variable name="quote-open-char">
            <xsl:variable name="rend">
                <xsl:choose>
                    <xsl:when test="contains(@rend, 'quoted-open-beginning')">
                        <xsl:value-of select="'quoted-open-beginning'"/>
                    </xsl:when>
                    <xsl:when test="contains(@rend, 'quoted')">
                        <xsl:value-of select="'quoted'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="''"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:call-template name="get-quote-char-environment">
                <xsl:with-param name="open-or-close" select="'open'"/>
                <xsl:with-param name="rend" select="$rend"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="quote-close-char">
            <xsl:variable name="rend">
                <xsl:choose>
                    <xsl:when test="contains(@rend, 'quoted-open-end')">
                        <xsl:value-of select="'quoted-open-end'"/>
                    </xsl:when>
                    <xsl:when test="contains(@rend, 'quoted')">
                        <xsl:value-of select="'quoted'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="''"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:call-template name="get-quote-char-environment">
                <xsl:with-param name="open-or-close" select="'close'"/>
                <xsl:with-param name="rend" select="$rend"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:copy-of select="$quote-open-char"/>
        <xsl:apply-templates/>
        <xsl:copy-of select="$quote-close-char"/>
    </xsl:template>
            
    <xsl:template name="get-quote-char">
        <xsl:param name="position"/>
        <xsl:variable name="quoted-count-self">
            <xsl:choose>
                <xsl:when test="contains(@rend, 'quoted')"><xsl:value-of select="1"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="0"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="quoted-count-up" select="count(ancestor::*[contains(@rend, 'quoted')])"/>            
        <xsl:variable name="quoted-count" select="$quoted-count-self + $quoted-count-up"/>
        <xsl:variable name="quote-char-type">
            <xsl:choose>
                <xsl:when test="$quoted-count = 0">
                    <xsl:value-of select="0"/>
                </xsl:when>
                <xsl:when test="$quoted-count mod 2 = 1">
                    <xsl:value-of select="1"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="2"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$quote-char-type = 0">
                <xsl:value-of select="''"/>
            </xsl:when>
            <xsl:when test="$position='open'">
                <xsl:choose>
                    <xsl:when test="ends-with(@xml:id, 'part-2')"><xsl:value-of select="''"/></xsl:when>
                    <xsl:when test="$quote-char-type = 1">
                        <xsl:value-of select="$double-quote-open"/>
                    </xsl:when>
                    <xsl:when test="$quote-char-type = 2">
                        <xsl:value-of select="$single-quote-open"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$position='close'">
                <xsl:choose>
                    <xsl:when test="ends-with(@xml:id, 'part-1')"><xsl:value-of select="''"/></xsl:when>
                    <xsl:when test="$quote-char-type = 1">
                        <xsl:value-of select="$double-quote-close"/>
                    </xsl:when>
                    <xsl:when test="$quote-char-type = 2">
                        <xsl:value-of select="$single-quote-close"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
        
    <xsl:template name="is-first-entity">
        <xsl:variable name="thisParentDivId" select="ancestor::div[@type = 'entry'][1]/@xml:id"/>
        <xsl:variable name="thisRef" select="@ref"/>
        <xsl:variable name="thisElName" select="local-name()"/>
        <xsl:choose>
            <xsl:when test="
                not(preceding::*[local-name()=$thisElName
                and @ref=$thisRef
                and ancestor::div[@xml:id=$thisParentDivId]])">
                <xsl:value-of select="true()"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="false()"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>
