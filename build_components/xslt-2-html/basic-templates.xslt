<xsl:stylesheet xmlns:cst="https://carlschmitt.bio/entityxml/terms#" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xpath-default-namespace="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="cst xs" version="3.0">
    
    <xsl:function name="cst:erase-zeros">
        <xsl:param name="str" as="xs:string"/>
        <xsl:analyze-string select="$str" regex="^0*">
            <xsl:matching-substring/>
            <xsl:non-matching-substring>
                <xsl:copy-of select="."/>
            </xsl:non-matching-substring>
        </xsl:analyze-string>
    </xsl:function>
    
    <xsl:template name="create-tei-xml-source-link">
        <xsl:param name="link"/>
        <div class="tei-xml-source-link">
            <a href="{$link}" target="_blank" title="Zur TEI XML-Quelldatei im Repositorium">{{ icons.code}}TEI XML</a>
        </div>        
    </xsl:template>
        
    <xsl:template name="getImageTextContent">
        <xsl:variable name="thisId" select="@xml:id"/>
        <xsl:copy-of select="./following-sibling::*[(local-name() = 'div' or local-name() = 'pb')
            and preceding-sibling::milestone[1]/@xml:id = $thisId]"/>
    </xsl:template>
    
</xsl:stylesheet>
