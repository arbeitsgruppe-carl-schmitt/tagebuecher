<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="cst xs" version="3.0"
    xmlns:cst="https://carlschmitt.bio/cst-ns" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:import href="basic-templates.xslt"/>
    <xsl:output indent="no" method="html" version="5"/>
    <xsl:template match="/">
        <div class="editorial-text">
            <section id="project-description">
                <h1>Das Projekt</h1>
                <xsl:apply-templates select="/teiCorpus/teiHeader/encodingDesc/projectDesc/*"/>
            </section>
            <section id="team">
                <h2>Die Arbeitsgruppe</h2>
                <div class="grid">
                    <span class="resp">Projektleitung und<br/>Herausgeberschaft</span>
                    <span class="name">
                        <xsl:for-each select="/teiCorpus/teiHeader/fileDesc/titleStmt/editor">
                            <xsl:choose>
                                <xsl:when test="starts-with(@ref, 'https://orcid.org')">
                                    <a class="orcid-link" href="{@ref}" target="_blank"><xsl:value-of select="text()"/>{{ icons.orcid }}</a>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="text()"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>
                    </span>
                    <xsl:for-each select="/teiCorpus/teiHeader/fileDesc/titleStmt/respStmt">
                        <span class="resp">
                            <xsl:value-of select="resp/text()"/>
                        </span>
                        <span class="name">
                            <xsl:for-each select="name">
                                <xsl:call-template name="create-person-entry-orcid-or-not"/>
                            </xsl:for-each>
                        </span>
                    </xsl:for-each>
                </div>
            </section>
        </div>
    </xsl:template>

    <xsl:template match="ab[@type = 'head']">
        <xsl:element name="{@subtype}">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    <xsl:template match="list">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="p">
        <xsl:if test="@n = 'passport-photo-anchor'">
            <figure>
                <img alt="Passfoto von Carl Schmitt, 1945"
                    src="imgs/CS_Passfoto_1945_RWB_32923a.avif" width="300"/>
                <figcaption>Passfoto von Carl Schmitt, 1945<br/>
                    Quelle: Nachlass, Sign. RWB  32923</figcaption>
            </figure>
        </xsl:if>
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="title">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    
    <xsl:template name="create-person-entry-orcid-or-not">
        <span class="person">
            <xsl:choose>
                <xsl:when test="starts-with(@ref, 'https://orcid.org')">
                    <a class="orcid-link" href="{@ref}" target="_blank"><xsl:value-of select="text()"/>{{ icons.orcid }}</a>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="text()"/>
                </xsl:otherwise>
            </xsl:choose>    
        </span>
    </xsl:template>
    
</xsl:stylesheet>
