<xsl:stylesheet version="3.0" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    exclude-result-prefixes="map xs"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output indent="no" method="xml"/>
    <xsl:mode on-no-match="shallow-copy"/>
    
    <xsl:variable name="sort-map" as="map(*)">
        <xsl:map>
            <xsl:map-entry key="'bible'" select="0"/>
            <xsl:map-entry key="'book'" select="1"/>
            <xsl:map-entry key="'chapter'" select="2"/>
            <xsl:map-entry key="'act'" select="3"/>
            <xsl:map-entry key="'scene'" select="4"/>
            <xsl:map-entry key="'verse'" select="5"/>
            <xsl:map-entry key="'line'" select="6"/>
            <xsl:map-entry key="'p'" select="7"/>
            <xsl:map-entry key="'para'" select="8"/>
            <xsl:map-entry key="'note'" select="9"/>
            <xsl:map-entry key="'footnote'" select="10"/>
        </xsl:map>
    </xsl:variable>
        
    <xsl:template match="app/note/rs[@type = 'document' and starts-with(@ref, '#__ZOTERO_')]">
        <xsl:call-template name="resolve-zotero-ref">
            <xsl:with-param name="ref" select="@ref"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="body//text()[preceding-sibling::node()[1][
        local-name()='quote'
        and starts-with(@source, '#__ZOTERO_')]]">
        <xsl:variable name="first-substring">
            <xsl:choose>
                <xsl:when test="matches(., '^[,.;)]')">
                    <xsl:analyze-string select="." regex="^([,.;)]{{1,2}})(.*)">
                        <xsl:matching-substring>
                            <xsl:value-of select="regex-group(1)" />
                        </xsl:matching-substring>
                    </xsl:analyze-string>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="remainder">
            <xsl:choose>
                <xsl:when test="matches(., '^[,.;)]')">
                    <xsl:analyze-string select="." regex="^([,.;)]{{1,2}})(.*)">
                        <xsl:matching-substring>
                            <xsl:value-of select="regex-group(2)" />
                        </xsl:matching-substring>
                    </xsl:analyze-string>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="$first-substring"/>
        <xsl:call-template name="create-quote-explanation"/>
        <xsl:value-of select="$remainder"/>
    </xsl:template>
    
    <xsl:template match="l[not(following-sibling::l)
        and parent::lg[starts-with(@source, '#__ZOTERO')]]">
        <xsl:copy-of select="."/>
        <app>
            <note type="quote-explanation">
                <xsl:call-template name="resolve-zotero-ref">
                    <xsl:with-param name="ref" select="parent::lg/@source"/>
                </xsl:call-template>
                <xsl:value-of select="'.'"/>
            </note>
        </app>
    </xsl:template>
    
    <!-- Named templates -->
    
    <xsl:template name="create-quote-explanation">
        <app>
            <note type="quote-explanation">
                <xsl:for-each select="preceding-sibling::quote[1]">
                    <xsl:value-of select="'Quelle: '"/>
                    <xsl:call-template name="resolve-zotero-ref">
                        <xsl:with-param name="ref" select="@source"/>
                    </xsl:call-template>
                    <xsl:value-of select="'.'"/>
                    <xsl:if test="app/lem[@type='origin']">
                        <xsl:value-of select="' Ursprünglicher Text: ' || app/lem[@type='origin']"/>
                    </xsl:if>
                    <xsl:if test="app/lem[@type='translation']">
                        <xsl:value-of select="' Übersetzung: ' || app/lem[@type='translation']"/>
                    </xsl:if>
                </xsl:for-each>
            </note>
        </app>
    </xsl:template>
    
    <xsl:template name="resolve-zotero-ref">
        <xsl:param name="ref"/>
        <xsl:variable name="tokenized-ref" select="tokenize($ref, '\?')"/>
        <xsl:for-each select="/teiCorpus/teiHeader/fileDesc/sourceDesc/listBibl[@xml:id = 'zotero-bibliography']/bibl[@xml:id = substring-after($tokenized-ref[1], '#')]">
            <bibl type="{@type}">
                <xsl:if test="author">
                    <xsl:call-template name="print-zotero-entities">
                        <xsl:with-param name="entity-name" select="'author'"/>
                    </xsl:call-template>
                    <xsl:value-of select="', '"/>
                </xsl:if>
                <xsl:call-template name="print-title"/>
                <xsl:choose>
                    <xsl:when test="@type = 'artwork'">
                        <xsl:call-template name="print-medium"/>
                        <xsl:call-template name="print-date-led-by-comma"/>
                        <xsl:call-template name="print-repository"/>
                    </xsl:when>
                    <xsl:when test="@type = 'book'">
                        <xsl:call-template name="print-edition"/>
                        <xsl:call-template name="print-editors"/>
                        <xsl:call-template name="print-pub-place"/>
                        <xsl:call-template name="print-publisher"/>
                        <xsl:call-template name="print-pub-date"/>
                        <xsl:call-template name="print-series-info"/>
                    </xsl:when>
                    <xsl:when test="@type = 'bookSection'">
                        <xsl:for-each select="bibl[@type = 'containing-publication']">
                            <xsl:value-of select="', in: '"/>
                            <xsl:call-template name="print-title"/>
                            <xsl:call-template name="print-editors"/>
                            <xsl:call-template name="print-pub-place"/>
                            <xsl:call-template name="print-pub-date"/>
                            <xsl:call-template name="print-page-scope"/>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="@type = 'journalArticle'">
                        <xsl:for-each select="bibl[@type = 'containing-publication']">
                            <xsl:value-of select="', in: '"/>
                            <xsl:call-template name="print-title"/>
                            <xsl:call-template name="print-date-led-by-comma"/>
                            <xsl:call-template name="print-volume"/>
                            <xsl:call-template name="print-issue"/>
                            <xsl:call-template name="print-page-scope"/>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="@type = 'letter'">
                        <xsl:call-template name="print-date-led-by-comma"/>
                        <xsl:call-template name="print-archive"/>
                        <xsl:call-template name="print-archive-location"/>
                    </xsl:when>
                    <xsl:when test="@type = 'magazineArticle'">
                        <xsl:for-each select="bibl[@type = 'containing-publication']">
                            <xsl:value-of select="', in: '"/>
                            <xsl:call-template name="print-title"/>
                            <xsl:call-template name="print-date-led-by-comma"/>
                            <xsl:call-template name="print-volume"/>
                            <xsl:call-template name="print-issue"/>
                            <xsl:call-template name="print-page-scope"/>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="@type = 'newspaperArticle'">
                        <xsl:for-each select="bibl[@type = 'containing-publication']">
                            <xsl:value-of select="', in: '"/>
                            <xsl:call-template name="print-title"/>
                            <xsl:call-template name="print-date-led-by-comma"/>
                            <xsl:call-template name="print-page-scope"/>
                        </xsl:for-each>
                    </xsl:when>
                </xsl:choose>
                <xsl:if test="$tokenized-ref[2]">
                    <xsl:value-of select="', '"/>
                    <xsl:if test="bibl[@type = 'containing-publication']/biblScope">
                        <xsl:value-of select="'hier '"/>
                    </xsl:if>
                    <xsl:variable name="source-citation-tokens" select="tokenize($tokenized-ref[2], '&amp;')"/>
                    <xsl:for-each select="$source-citation-tokens">
                        <xsl:sort select="map:get($sort-map, substring-before(., '='))"/>
                        <xsl:variable name="key" select="substring-before(., '=')"/>
                        <xsl:variable name="value" select="substring-after(., '=')"/>
                        <xsl:if test="position() != 1">
                            <xsl:value-of select="', '"/>
                        </xsl:if>
                        <xsl:choose>
                            <xsl:when test="$key = 'act'">
                                <xsl:value-of select="$value || '. Akt'"/>
                            </xsl:when>
                            <xsl:when test="$key = 'bible'">
                                <xsl:analyze-string select="$value" regex="^([1-3]?)([A-Za-z]+)([0-9]+)(?:,([0-9]+))?(?:-([0-9]+))?$">
                                    <xsl:matching-substring>
                                        <xsl:variable name="part1" select="if (regex-group(1)) then concat(regex-group(1), ' ') else ''" />
                                        <xsl:variable name="part2" select="regex-group(2)" />
                                        <xsl:variable name="part3" select="concat(' ', regex-group(3))" />
                                        <xsl:variable name="part4" select="if (regex-group(4)) then concat(', ', regex-group(4)) else ''" />
                                        <xsl:variable name="part5" select="if (regex-group(5)) then concat('-', regex-group(5)) else ''" />
                                        <xsl:value-of select="$part1 || $part2 || $part3 || $part4 || $part5" />
                                    </xsl:matching-substring>
                                    <xsl:non-matching-substring>
                                        <xsl:value-of select="''" />
                                    </xsl:non-matching-substring>
                                </xsl:analyze-string>
                            </xsl:when>
                            <xsl:when test="$key = 'book'">
                                <xsl:value-of select="$value || '. Buch'"/>
                            </xsl:when>
                            <xsl:when test="$key = 'chapter'">
                                <xsl:value-of select="$value || '. Kapitel'"/>
                            </xsl:when>
                            <xsl:when test="$key = 'footnote'">
                                <xsl:value-of select="'Fußnote ' || $value"/>
                            </xsl:when>
                            <xsl:when test="$key = 'line'">
                                <xsl:value-of select="'Zeile ' || $value"/>
                            </xsl:when>
                            <xsl:when test="$key = 'note'">
                                <xsl:value-of select="'Anm. ' || $value"/>
                            </xsl:when>
                            <xsl:when test="$key = 'p'">
                                <xsl:value-of select="'S. ' || $value"/>
                            </xsl:when>
                            <xsl:when test="$key = 'para'">
                                <xsl:value-of select="$value || '. Absatz'"/>
                            </xsl:when>
                            <xsl:when test="$key = 'scene'">
                                <xsl:value-of select="$value || '. Szene'"/>
                            </xsl:when>
                            <xsl:when test="$key = 'verse'">
                                <xsl:value-of select="$value || '. Strophe'"/>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:for-each>
                </xsl:if>
            </bibl>
        </xsl:for-each>
    </xsl:template>
    
    <!-- Templates to print Zotero values -->
    
    <xsl:template name="print-archive">
        <xsl:if test="note[@type = 'archive']">
            <xsl:value-of select="', ' || note[@type = 'archive']"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="print-archive-location">
        <xsl:if test="note[@type = 'archive-location']">
            <xsl:value-of select="', ' || note[@type = 'archive-location']"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="print-date-led-by-comma">
        <xsl:if test="date/text()">
            <xsl:value-of select="', ' || date/text()"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="print-edition">
        <xsl:if test="edition">
            <xsl:value-of select="', ' || edition"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="print-editors">
        <xsl:if test="editor">
            <xsl:value-of select="', hg. von '"/>
            <xsl:call-template name="print-zotero-entities">
                <xsl:with-param name="entity-name" select="'editor'"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="print-issue">
        <xsl:if test="biblScope[@unit = 'issue']">
            <xsl:value-of select="', H. ' || biblScope[@unit = 'issue']"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="print-medium">
        <xsl:if test="note[@type = 'artwork-medium']">
            <xsl:value-of select="' (' || note[@type = 'artwork-medium']"/>
            <xsl:if test="note[@type = 'artwork-size']">
                <xsl:value-of select="', ' || note[@type = 'artwork-size']"/>
            </xsl:if>
            <xsl:value-of select="')'"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="print-page-scope">
        <xsl:if test="biblScope[@unit = 'page']">
            <xsl:value-of select="', S. ' || biblScope[@unit = 'page']"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="print-pub-place">
        <xsl:if test="pubPlace">
            <xsl:value-of select="', '"/>
            <xsl:call-template name="print-zotero-entities">
                <xsl:with-param name="entity-name" select="'pubPlace'"/>
            </xsl:call-template>
            <xsl:if test="publisher">
                <xsl:value-of select="': '"/>
            </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="print-publisher">
        <xsl:value-of select="publisher"/>
    </xsl:template>
    
    <xsl:template name="print-pub-date">
        <xsl:value-of select="' ' || date"/>
    </xsl:template>
    
    <xsl:template name="print-repository">
        <xsl:if test="note[@artwork-repository]">
            <xsl:value-of select="', Bestand: ' || note[@artwork-repository]"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="print-series-info">
        <xsl:if test="title[@type = 'series']">
            <xsl:value-of select="' ('"/>
            <title type="series">
                <xsl:value-of select="title[@type = 'series']"/>
            </title>
            <xsl:if test="num[@type = 'series']">
                <xsl:value-of select="' ' || num[@type = 'series']"/>
            </xsl:if>
            <xsl:value-of select="')'"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="print-title">
        <title>
            <xsl:value-of select="title[@type = 'main']"/>
        </title>
    </xsl:template>
    
    <xsl:template name="print-volume">
        <xsl:if test="biblScope[@unit = 'volume']">
            <xsl:value-of select="', Bd. ' || biblScope[@unit = 'volume']"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="print-zotero-entities">
        <xsl:param name="entity-name"/>
        <xsl:for-each select="*[local-name() = $entity-name]">
            <xsl:choose>
                <xsl:when test="$entity-name = 'author'">
                    <author>
                        <xsl:value-of select="."/>
                    </author>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="count(following-sibling::*[local-name() = $entity-name]) gt 1">
                    <xsl:value-of select="', '"/>
                </xsl:when>
                <xsl:when test="count(following-sibling::*[local-name() = $entity-name]) = 1">
                    <xsl:value-of select="' und '"/>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
    
</xsl:stylesheet>
