<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.tei-c.org/ns/1.0" xpath-default-namespace="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="3.0">
    <xsl:output method="xml" indent="no"/>
    <xsl:mode on-no-match="shallow-copy"/>
    <xsl:template match="processing-instruction('oxy_comment_start')"/>
    <xsl:template match="processing-instruction('oxy_comment_end')"/>
    <xsl:template match="comment()"/>
</xsl:stylesheet>
