<xsl:stylesheet
    exclude-result-prefixes="cst entx gndo tei xs"
    version="3.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:cst="https://carlschmitt.bio/entityxml/terms#"
    xmlns:entx="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:import href="../xslt-2-html/basic-templates.xslt"/>
    <xsl:output indent="yes" method="xml"/>
    
    <xsl:param name="entity"/>
    <xsl:param name="entities_list_path"/>
    <xsl:variable name="MAX_CONTEXT_WORD_COUNT" select="11"/>
    <xsl:variable
        name="entities"
        select="document($entities_list_path)/entx:entityXML/entx:collection/entx:data/entx:list/*[local-name() = $entity]"
        as="element()*"/>
    <xsl:variable
        name="source-document"
        select="/teiCorpus"
        as="element()"/>
    <xsl:variable
        name="entities-list"
        as="element()">
        <xsl:element name="{'list' || upper-case(substring($entity, 1, 1)) || substring($entity, 2)}" inherit-namespaces="no">
            <xsl:for-each select="$entities">
                <xsl:sort select="cst:sortkey/text()"/>
                <xsl:variable name="ref">
                    <xsl:choose>
                        <xsl:when test="@gndo:uri">
                            <xsl:value-of select="'#__GND_' || substring-after(@gndo:uri, 'https://d-nb.info/gnd/')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'#' || @xml:id"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <person xml:id="{@xml:id}" sortKey="{cst:sortkey/text()}">
                    <name type="display">
                        <xsl:choose>
                            <xsl:when test="cst:preferredDisplayName/text()">
                                <xsl:value-of select="cst:preferredDisplayName[1]/text()"/>
                            </xsl:when>
                            <xsl:when test="gndo:preferredName/gndo:personalName">
                                <xsl:value-of select="gndo:preferredName/gndo:personalName"/>
                            </xsl:when>
                            <xsl:when test="gndo:preferredName/gndo:forename/text() and gndo:preferredName/gndo:surname/text()">
                                <xsl:value-of select="gndo:preferredName/gndo:forename/text() || ' ' || gndo:preferredName/gndo:surname/text()"/>
                            </xsl:when>
                            <xsl:when test="gndo:preferredName/gndo:forename and gndo:preferredName/gndo:surname">
                                <xsl:value-of select="gndo:preferredName/gndo:forename/text() || gndo:preferredName/gndo:surname/text()"/>
                            </xsl:when>
                        </xsl:choose>
                    </name>
                    <name type="index">
                        <xsl:choose>
                            <xsl:when test="cst:preferredIndexName/text()">
                                <xsl:value-of select="cst:preferredIndexName[1]/text()"/>
                            </xsl:when>
                            <xsl:when test="gndo:preferredName/gndo:personalName">
                                <xsl:value-of select="gndo:preferredName/gndo:personalName"/>
                            </xsl:when>
                            <xsl:when test="gndo:preferredName/gndo:forename/text() and gndo:preferredName/gndo:surname/text()">
                                <xsl:value-of select="gndo:preferredName/gndo:surname/text() || ', ' || gndo:preferredName/gndo:forename/text()"/>
                            </xsl:when>
                            <xsl:when test="gndo:preferredName/gndo:forename/text() or gndo:preferredName/gndo:surname/text()">
                                <xsl:value-of select="gndo:preferredName/gndo:surname/text() || gndo:preferredName/gndo:forename/text()"/>
                            </xsl:when>
                        </xsl:choose>
                    </name>
                    <xsl:if test="gndo:dateOfBirth/@iso-date">
                        <birth>
                            <xsl:call-template name="add-date-values">
                                <xsl:with-param name="el-name" select="'gndo:dateOfBirth'"/>
                            </xsl:call-template>
                        </birth>
                    </xsl:if>
                    <xsl:if test="gndo:dateOfDeath/@iso-date">
                        <death>
                            <xsl:call-template name="add-date-values">
                                <xsl:with-param name="el-name" select="'gndo:dateOfDeath'"/>
                            </xsl:call-template>
                        </death>
                    </xsl:if>
                    <xsl:if test="gndo:biographicalOrHistoricalInformation">
                        <note type="gndo:biographicalOrHistoricalInformation"><xsl:value-of select="gndo:biographicalOrHistoricalInformation"/></note>
                    </xsl:if>
                    <xsl:if test="cst:info">
                        <note type="cst:info"><xsl:value-of select="cst:info"/></note>
                    </xsl:if>
                    <xsl:variable name="occurences" as="element()*">
                        <xsl:for-each select="$source-document/TEI/text/body/div[@type='entry']//rs[@type=$entity and @ref=$ref]">
                            <xsl:variable name="ancestor-entry-id" select="ancestor::div[@type='entry'][1]/@xml:id"/>
                            <xsl:if test="not(preceding::rs[ancestor::div[@type='entry'][1]/@xml:id=$ancestor-entry-id and @ref=$ref])">
                                <xsl:variable name="info-strings" select="tokenize($ancestor-entry-id, '_')" as="xs:string*"/>
                                <xsl:variable name="is-dated">
                                    <xsl:choose>
                                        <xsl:when test="$info-strings[3] castable as xs:date"><xsl:value-of select="true()"/></xsl:when>
                                        <xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:variable name="date-label">
                                    <xsl:choose>
                                        <xsl:when test="$is-dated">
                                            <xsl:value-of select="format-date(xs:date($info-strings[3]), '[D].[M].[Y]')"/>
                                        </xsl:when>
                                        <xsl:otherwise/>
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:variable name="entry-type-label">
                                    <xsl:choose>
                                        <xsl:when test="$info-strings[2] = 'calendar'">
                                            <xsl:value-of select="'Kalendereintrag'"/>
                                        </xsl:when>
                                        <xsl:when test="$info-strings[2] = 'diary-entry'">
                                            <xsl:value-of select="'Tagebucheintrag'"/>
                                        </xsl:when>
                                        <xsl:when test="$info-strings[2] = 'excerpt'">
                                            <xsl:value-of select="'Exzerpt'"/>
                                        </xsl:when>
                                        <xsl:when test="$info-strings[2] = 'letter-draft'">
                                            <xsl:value-of select="'Briefentwurf'"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="'Eintrag unbekannter Art'"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:variable name="sortkey">
                                    <xsl:choose>
                                        <xsl:when test="$is-dated = true()">
                                            <xsl:value-of select="$info-strings[3] || '_' || $info-strings[1]"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="'z_' || $ancestor-entry-id"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <f name="occurence">
                                    <fs>
                                        <f name="containing-book"><xsl:value-of select="$info-strings[1]"/></f>
                                        <xsl:if test="$is-dated='true'">
                                            <f name="date-label"><xsl:value-of select="$date-label"/></f>
                                        </xsl:if>
                                        <f name="entry-id"><xsl:value-of select="$ancestor-entry-id"/></f>
                                        <f name="entry-type-label"><xsl:value-of select="$entry-type-label"/></f>
                                        <f name="following-context">
                                            <xsl:call-template name="get-context">
                                                <xsl:with-param name="ancestor-id" select="ancestor::ab[1]/@xml:id"/>
                                                <xsl:with-param name="direction" select="'forward'"/>
                                                <xsl:with-param name="i" select="0"/>
                                                <xsl:with-param name="text" select="''"/>
                                            </xsl:call-template>
                                        </f>
                                        <f name="id"><xsl:value-of select="@xml:id"/></f>
                                        <f name="is-dated"><xsl:value-of select="string($is-dated)"/></f>
                                        <f name="text-content"><xsl:value-of select="replace(string-join(.//text()[not(parent::expan)]), '\s+', ' ')"/></f>
                                        <f name="preceding-context">
                                            <xsl:call-template name="get-context">
                                                <xsl:with-param name="ancestor-id" select="ancestor::ab[1]/@xml:id"/>
                                                <xsl:with-param name="direction" select="'backward'"/>
                                                <xsl:with-param name="i" select="0"/>
                                                <xsl:with-param name="text" select="''"/>
                                            </xsl:call-template>
                                        </f>
                                        <f name="sortkey"><xsl:value-of select="$sortkey"/></f>
                                        <f name="type">explicit</f>
                                    </fs>
                                </f>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:variable>
                    <fs type="occurences">
                        <xsl:for-each select="$occurences">
                            <xsl:sort select="./fs/f[@name='sortkey']/text()"/>
                            <xsl:copy-of select="."/>
                        </xsl:for-each>
                    </fs>
                </person>
            </xsl:for-each>
        </xsl:element>
    </xsl:variable>
        
    <xsl:template match="/">
        <xsl:copy-of select="$entities-list"/>
    </xsl:template>
    
    <xsl:template name="add-date-values">
        <xsl:param name="el-name"/>
        <xsl:choose>
            <xsl:when test="*[name() = $el-name]/@iso-date castable as xs:date">
                <xsl:attribute name="when-iso" select="*[name() = $el-name]/@iso-date"/>
                <xsl:value-of select="format-date(*[name() = $el-name]/@iso-date, '[D].[M].[Y]')"/>
            </xsl:when>
            <xsl:when test="matches(*[name() = $el-name]/@iso-date, '^[0-9]{1,4}$')">
                <xsl:attribute name="when" select="*[name() = $el-name]/@iso-date"/>
                <xsl:value-of select="cst:erase-zeros(*[name() = $el-name]/@iso-date)"/>
            </xsl:when>
            <xsl:when test="matches(*[name() = $el-name]/@iso-date, '^[-]{1}[0-9]{1,4}$')">
                <xsl:attribute name="when" select="*[name() = $el-name]/@iso-date"/>
                <xsl:value-of select="cst:erase-zeros(*[name() = $el-name]/substring-after(@iso-date, '-')) || ' v. Chr.'"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="get-context">
        <xsl:param name="ancestor-id"/>
        <xsl:param name="direction"/>
        <xsl:param name="i"/>
        <xsl:param name="text"/>
        <xsl:variable name="text-len" select="count(tokenize($text, ' '))"/>
        <xsl:variable name="axis">
            <xsl:choose>
                <xsl:when test="$direction = 'backward'">
                    <xsl:value-of select="'preceding'"/>
                </xsl:when>
                <xsl:when test="$direction = 'forward'">
                    <xsl:value-of select="'following'"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="next-fragment-ancestor-id">
            <xsl:choose>
                <xsl:when test="$direction = 'backward'">
                    <xsl:value-of select="preceding::text()[$i + 1]/ancestor::ab[1]/@xml:id"/>
                </xsl:when>
                <xsl:when test="$direction = 'forward'">
                    <xsl:value-of select="following::text()[$i + 1]/ancestor::ab[1]/@xml:id"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$text-len gt ($MAX_CONTEXT_WORD_COUNT - 1)
                or $next-fragment-ancestor-id != $ancestor-id">
                <xsl:call-template name="normalize-context">
                    <xsl:with-param name="direction" select="$direction"/>
                    <xsl:with-param name="text" select="$text"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="next-fragment">
                    <xsl:evaluate xpath="'./' || $axis || '::text()[' || $i + 1 || ']
                        [not(parent::del)
                        and not(parent::expan)
                        and not(parent::rdg)
                        and not(parent::c[@type=''ephemeral-hyphen''])
                        and not(ancestor::note[parent::app])]'"
                        context-item="."/>
                </xsl:variable>
                <xsl:variable name="more-text">
                    <xsl:choose>
                        <xsl:when test="$direction = 'backward'">
                            <xsl:value-of select="$next-fragment || $text"/>
                        </xsl:when>
                        <xsl:when test="$direction = 'forward'">
                            <xsl:value-of select="$text || $next-fragment"/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:variable>
                <xsl:call-template name="get-context">
                    <xsl:with-param name="ancestor-id" select="$ancestor-id"/>
                    <xsl:with-param name="direction" select="$direction"/>
                    <xsl:with-param name="i" select="$i + 1"/>
                    <xsl:with-param name="text" select="replace($more-text, '\s+', ' ')"/>  
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>      
    </xsl:template>
     
    <xsl:template name="normalize-context">
        <xsl:param name="direction"/>
        <xsl:param name="text"/>
        <xsl:variable name="trunc-text">
            <xsl:choose>
                <xsl:when test="$direction = 'backward'">
                    <xsl:value-of select="replace($text, '^\s+', '')"/>
                </xsl:when>
                <xsl:when test="$direction = 'forward'">
                    <xsl:value-of select="replace($text, '\s+$', '')"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>  
        <xsl:variable name="words" select="tokenize($trunc-text, ' ')" as="xs:string*"/>
        <xsl:choose>
            <xsl:when test="$direction = 'backward'">
                <xsl:variable name="start-position" select="max((1, count($words) - $MAX_CONTEXT_WORD_COUNT + 1))"/>
                <xsl:value-of select="string-join(subsequence($words, $start-position), ' ')"/>
            </xsl:when>
            <xsl:when test="$direction = 'forward'">
                <xsl:value-of select="string-join(subsequence($words, 1, $MAX_CONTEXT_WORD_COUNT), ' ')" />
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>
