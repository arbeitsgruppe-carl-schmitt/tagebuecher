<xsl:stylesheet
    exclude-result-prefixes="cst tei xs"
    version="3.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:cst="https://carlschmitt.bio/entityxml/terms#"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <!-- <xsl:import href="../xslt-2-html/basic-templates.xslt"/> -->
    <xsl:output method="text"/>
    
    <xsl:variable name="entities-map" as="element()">
        <array xmlns="http://www.w3.org/2005/xpath-functions">
                <xsl:for-each select="/listPerson/person">
                    <map>
                        <string key="id"><xsl:value-of select="@xml:id"/></string>
                        <xsl:if test="starts-with(@xml:id, '__GND_')">
                            <string key="gnd_id"><xsl:value-of select="substring-after(@xml:id, '__GND_')"/></string>
                        </xsl:if>
                        <string key="display_name"><xsl:value-of select="name[@type='display']"/></string>
                        <string key="index_name"><xsl:value-of select="name[@type='index']"/></string>
                        <string key="birth_date">
                            <xsl:choose>
                                <xsl:when test="birth/text()">
                                    <xsl:value-of select="birth/text()"/>
                                </xsl:when>
                                <xsl:otherwise><xsl:value-of select="'?'"/></xsl:otherwise>
                            </xsl:choose>
                        </string>
                        <string key="death_date">
                            <xsl:choose>
                                <xsl:when test="death/text()">
                                    <xsl:value-of select="death/text()"/>
                                </xsl:when>
                                <xsl:otherwise><xsl:value-of select="'?'"/></xsl:otherwise>
                            </xsl:choose>
                        </string>
                        <xsl:if test="note[@type='cst:info']/text()">
                            <string key="cst_info"><xsl:value-of select="note[@type='cst:info']"/></string>
                        </xsl:if>
                        <xsl:if test="note[@type='gndo:biographicalOrHistoricalInformation']/text()">
                            <string key="gndo_biographicalOrHistoricalInformation"><xsl:value-of select="replace(note[@type='gndo:biographicalOrHistoricalInformation']/text(), '&lt;', '&amp;lt;')"/></string>
                        </xsl:if>
                        <boolean key="is_gnd_entry">
                            <xsl:choose>
                                <xsl:when test="starts-with(@xml:id, '__GND_')"><xsl:value-of select="true()"/></xsl:when>
                                <xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
                            </xsl:choose>
                        </boolean>
                        <string key="sortkey"><xsl:value-of select="cst:sortkey"/></string>
                        <array key="occurences">
                            <xsl:for-each select="fs[@type='occurences']/f[@name='occurence']">
                                <map>
                                    <string key="containing_book"><xsl:value-of select="fs/f[@name='containing-book']"/></string>
                                    <string key="date_label"><xsl:value-of select="fs/f[@name='date-label']"/></string>
                                    <string key="entry_id"><xsl:value-of select="fs/f[@name='entry-id']"/></string>
                                    <string key="entry_type"><xsl:value-of select="fs/f[@name='entry-type-label']"/></string>
                                    <string key="following_context"><xsl:value-of select="fs/f[@name='following-context']"/></string>
                                    <string key="id"><xsl:value-of select="fs/f[@name='id']"/></string>
                                    <boolean key="is_dated"><xsl:value-of select="fs/f[@name='is-dated']"/></boolean>
                                    <string key="preceding_context"><xsl:value-of select="fs/f[@name='preceding-context']"/></string>
                                    <string key="sortkey"><xsl:value-of select="fs/f[@name='sortkey']"/></string>
                                    <string key="text_content"><xsl:value-of select="fs/f[@name='text-content']"/></string>
                                    <string key="type"><xsl:value-of select="fs/f[@name='type']"/></string>
                                </map>
                            </xsl:for-each>
                        </array>
                    </map>
                </xsl:for-each>
            </array>
    </xsl:variable>
    
    <xsl:template match="/">
        <xsl:value-of select="xml-to-json($entities-map)"/>
    </xsl:template>
    
</xsl:stylesheet>
