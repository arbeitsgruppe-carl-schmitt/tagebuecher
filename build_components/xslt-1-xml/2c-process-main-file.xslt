<xsl:stylesheet version="3.0" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    exclude-result-prefixes="map xs"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output indent="no" method="xml"/>
    <xsl:mode on-no-match="shallow-copy"/>
        
    <xsl:template match="rs[@type = 'person' or @type = 'work' or @type = 'place']
        | quote[not(@xml:id)]
        | text//ab[not(@xml:id)]
        | text//lg[not(@xml:id)]
        | text//note[not(@xml:id)]">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:attribute name="xml:id" select="generate-id()"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
