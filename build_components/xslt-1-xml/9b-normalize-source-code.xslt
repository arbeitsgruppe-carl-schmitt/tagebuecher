<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    version="3.0"
    exclude-result-prefixes="tei xs"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:mode on-no-match="shallow-copy"/>
    
    <xsl:template match="rs[@ref and ancestor::div[@type='entry' and @xml:id]]">
        <xsl:variable name="this-ancestor-id" select="ancestor::div[@type='entry' and @xml:id][1]/@xml:id"/>
        <xsl:variable name="this-ref" select="@ref"/>
        <xsl:choose>
            <xsl:when test="not(preceding::rs[ancestor::div[@type='entry' and @xml:id][1]/@xml:id = $this-ancestor-id
                and @ref=$this-ref])">
                <xsl:copy-of select="."/>
            </xsl:when>
            <xsl:when test="@rend='longhand'">
                <xsl:element name="seg" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="rend" select="'longhand'"/>
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>


