<xsl:stylesheet version="3.0" xmlns="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output indent="no" method="xml"/>
    <xsl:mode on-no-match="shallow-copy"/>

    <xsl:param name="bookUris" select="bookUris"/>
    <xsl:variable as="xs:string*" name="bookUrisArray" select="tokenize($bookUris, ',')"/>
    <xsl:param name="zotero_bibliography_path" select="zotero_bibliography_path"/>

    <xsl:template match="/">
        <xsl:apply-templates select="processing-instruction('xml-model')"/>
        <xsl:apply-templates select="processing-instruction('xml-stylesheet')"/>
        <teiCorpus xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:apply-templates select="/TEI/teiHeader"/>
            <xsl:for-each select="$bookUrisArray">
                <xsl:copy-of select="document(.)/TEI"/>
            </xsl:for-each>

        </teiCorpus>
    </xsl:template>

    <xsl:template match="publicationStmt/p/date">
        <xsl:copy>
            <xsl:value-of select="current-dateTime()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="sourceDesc">
        <xsl:copy>
            <listBibl xml:id="zotero-bibliography">
                <xsl:apply-templates select="document($zotero_bibliography_path)/listBibl/*"/>
            </listBibl>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
