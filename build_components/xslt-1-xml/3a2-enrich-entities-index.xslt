<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="rdf rdfs xs" version="3.0"
   xmlns:cst="https://carlschmitt.bio/entityxml/terms#"
   xmlns:entx="https://sub.uni-goettingen.de/met/standards/entity-xml#"
   xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
   xmlns:tei="http://www.tei-c.org/ns/1.0"
   xmlns:xs="http://www.w3.org/2001/XMLSchema"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:function name="cst:normalize-sortkey">
      <xsl:param name="input-string" as="xs:string"/>
      <xsl:variable name="no-whitespace" select="replace($input-string, '\s+', '')"/>
      <xsl:variable name="no-punctuation" select="translate($no-whitespace, ' .,-', '')"/>
      <xsl:sequence select="upper-case(translate($no-punctuation, 'ÁÄÉÖÜ', 'AAEOU'))"/>
   </xsl:function>
   <xsl:mode on-no-match="shallow-copy"/>
   <xsl:output indent="yes" method="xml"/>
   <xsl:param name="entities-data-path" select="entities-data-path"/>
   <xsl:param name="entity" select="entity"/>
   <xsl:param name="persons-entities-file-path" select="persons-entities-file-path"/>
   <xsl:variable name="persons-info">
      <xsl:if test="$entity = 'work'">
         <xsl:copy-of select="document($persons-entities-file-path)/entx:entityXML/entx:collection/entx:data/entx:list/entx:person"/>
      </xsl:if>
   </xsl:variable>
   
   <xsl:variable name="A_list">
      <xsl:for-each select="/entx:entityXML/entx:collection/entx:data/entx:list/*[local-name()=$entity]">
         <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:choose>
               <xsl:when test="@gndo:uri">
                  <xsl:variable name="this-id" select="tokenize(@gndo:uri, '/')[last()]"/>
                  <xsl:variable name="gnd-dataset" as="node()" select="document($entities-data-path || '/' || $this-id || '.xml')"/>
                  <xsl:choose>
                     <xsl:when test="$entity='person'">
                        <xsl:call-template name="process-person-entry">
                           <xsl:with-param name="gnd-dataset" select="$gnd-dataset"/>
                        </xsl:call-template>
                     </xsl:when>
                     <xsl:when test="$entity='work'">
                        <xsl:call-template name="process-work-entry">
                           <xsl:with-param name="gnd-dataset" select="$gnd-dataset"/>
                        </xsl:call-template>
                     </xsl:when>
                  </xsl:choose>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:apply-templates select="node()"/>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:copy>
      </xsl:for-each>      
   </xsl:variable>
   
   <xsl:variable name="B_list">
      <xsl:for-each select="$A_list/*[local-name()=$entity]">
         <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates select="node()[name()!='cst:sortkey' and name()!='revision']"/>
            <cst:sortkey><xsl:call-template name="get-sortkey"/></cst:sortkey>
            <xsl:apply-templates select="entx:revision"/>
         </xsl:copy>
      </xsl:for-each>
   </xsl:variable>
   
   <xsl:variable name="C_list">
      <xsl:if test="$B_list/*[starts-with(@xml:id, '__GND')]">
         <xsl:text>&#10;</xsl:text>
         <xsl:comment> ***&#10;The following section contains entities which already exist in the GND.&#10;*** </xsl:comment>
         <xsl:text>&#10;</xsl:text>
         <xsl:for-each select="$B_list/*[starts-with(@xml:id, '__GND')]">
            <xsl:sort select="cst:sortkey/text()" data-type="text"/>
            <xsl:copy-of select="."/>
         </xsl:for-each>
      </xsl:if>
      <xsl:if test="$B_list/*[not(starts-with(@xml:id, '__GND'))
         and child::entx:revision]">
         <xsl:text>&#10;</xsl:text>
         <xsl:comment> ***&#10;The following section contains entities with our own internal IDs on which we already started working.&#10;*** </xsl:comment>
         <xsl:text>&#10;</xsl:text>
         <xsl:for-each select="$B_list/*[not(starts-with(@xml:id, '__GND'))
         and child::entx:revision]">
            <xsl:sort select="cst:sortkey/text()" data-type="text"/>
            <xsl:copy-of select="."/>
         </xsl:for-each>
      </xsl:if>
      <xsl:if test="$B_list/*[not(starts-with(@xml:id, '__GND'))
         and not(child::entx:revision)]">
         <xsl:text>&#10;</xsl:text>
         <xsl:comment> ***&#10;The following section contains entities with our own internal IDs which we didn't touch yet.&#10;*** </xsl:comment>
         <xsl:text>&#10;</xsl:text>
         <xsl:for-each select="$B_list/*[not(starts-with(@xml:id, '__GND'))
            and not(child::entx:revision)]">
            <xsl:sort select="cst:sortkey/text()" data-type="text"/>
            <xsl:copy-of select="."/>
         </xsl:for-each>
      </xsl:if>
   </xsl:variable>
 
   <xsl:template match="entx:list">
      <xsl:copy>
         <xsl:apply-templates select="@*"/>
         <xsl:copy-of select="$C_list"/>
      </xsl:copy>
   </xsl:template>
   
   <xsl:template name="get-basic-info">
      <xsl:param name="gnd-dataset"/>
      <xsl:if test="$gnd-dataset/rdf:RDF/*/gndo:biographicalOrHistoricalInformation/text()">
         <xsl:for-each select="$gnd-dataset/rdf:RDF/*/gndo:biographicalOrHistoricalInformation">
            <gndo:biographicalOrHistoricalInformation><xsl:value-of select="text()"/></gndo:biographicalOrHistoricalInformation>
         </xsl:for-each>
      </xsl:if>
   </xsl:template>
   <xsl:template name="get-cst-data">
      <xsl:if test="cst:info">
         <xsl:copy-of select="cst:info"/>
      </xsl:if>
   </xsl:template>
   
   <xsl:template name="get-sortkey">
      <xsl:choose>
         <xsl:when test="$entity = 'person'">
            <xsl:variable name="sortkey">
               <xsl:choose>
                  <xsl:when test="cst:preferredSortkey/text()">
                     <xsl:value-of select="cst:preferredSortkey[1]/text()"/>
                  </xsl:when>
                  <xsl:when test="gndo:preferredName/gndo:forename/text()='N.' and gndo:preferredName/gndo:surname/text()='N.'">
                     <xsl:value-of select="@xml:id"/>
                  </xsl:when>
                  <xsl:when test="gndo:preferredName//text()">
                     <xsl:value-of select="string-join(gndo:preferredName//text())"/>
                  </xsl:when>
                  <xsl:otherwise><xsl:value-of select="@xml:id"/></xsl:otherwise>
               </xsl:choose>
            </xsl:variable>
            <xsl:value-of select="cst:normalize-sortkey($sortkey)"/>
         </xsl:when>
         <xsl:when test="$entity = 'work'">
            <xsl:choose>
               <xsl:when test="@gndo:uri and gndo:author/@gndo:ref">
                  <xsl:variable name="author-name-part">
                     <xsl:for-each select="gndo:author[@gndo:ref][1]">
                        <xsl:variable name="author-uri" select="@gndo:ref"/>
                        <xsl:if test="$persons-info/entx:person[@gndo:uri = $author-uri]">
                           <xsl:value-of select="cst:normalize-sortkey(string-join($persons-info/entx:person[@gndo:uri = $author-uri]/gndo:preferredName//text()))"/>
                        </xsl:if>
                     </xsl:for-each>
                  </xsl:variable>
                  <xsl:variable name="work-name-part" select="cst:normalize-sortkey(gndo:preferredName)"/>
                  <xsl:value-of select="$author-name-part || $work-name-part"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="@xml:id"/>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:when>
      </xsl:choose>
   </xsl:template>
   
   <xsl:template name="process-person-entry">
      <xsl:param name="gnd-dataset"/>
      <xsl:if test="cst:preferredDisplayName/text()">
         <xsl:copy-of select="cst:preferredDisplayName[1]"/>
      </xsl:if>
      <xsl:if test="cst:preferredIndexName/text()">
         <xsl:copy-of select="cst:preferredIndexName[1]"/>
      </xsl:if>
      <xsl:if test="cst:preferredSortkey/text()">
         <xsl:copy-of select="cst:preferredSortkey[1]"/>
      </xsl:if>
      <xsl:choose>
         <xsl:when test="$gnd-dataset/rdf:RDF/*/gndo:preferredNameEntityForThePerson/rdf:Description/gndo:surname/text()
            and $gnd-dataset/rdf:RDF/*/gndo:preferredNameEntityForThePerson/rdf:Description/gndo:forename/text()">
            <gndo:preferredName>
               <gndo:surname><xsl:value-of select="$gnd-dataset/rdf:RDF/*/gndo:preferredNameEntityForThePerson/rdf:Description/gndo:surname/text()"/></gndo:surname>
               <gndo:forename><xsl:value-of select="$gnd-dataset/rdf:RDF/*/gndo:preferredNameEntityForThePerson/rdf:Description/gndo:forename/text()"/></gndo:forename>
            </gndo:preferredName>
         </xsl:when>
         <xsl:when test="$gnd-dataset/rdf:RDF/*/gndo:preferredNameForThePerson/text()">
            <gndo:preferredName>
               <gndo:personalName><xsl:value-of select="$gnd-dataset/rdf:RDF/*/gndo:preferredNameForThePerson/text()"/></gndo:personalName>
            </gndo:preferredName>
         </xsl:when>
      </xsl:choose>
      <xsl:if test="$gnd-dataset/rdf:RDF/*/gndo:dateOfBirth/text() castable as xs:date
         or matches($gnd-dataset/rdf:RDF/*/gndo:dateOfBirth/text(), '^[-]?[0-9]{4}')">
         <gndo:dateOfBirth iso-date="{$gnd-dataset/rdf:RDF/*/gndo:dateOfBirth/text()}"/>
      </xsl:if>
      <xsl:if test="$gnd-dataset/rdf:RDF/*/gndo:dateOfDeath/text() castable as xs:date
         or matches($gnd-dataset/rdf:RDF/*/gndo:dateOfDeath/text(), '^[-]?[0-9]{4}')">
         <gndo:dateOfDeath iso-date="{$gnd-dataset/rdf:RDF/*/gndo:dateOfDeath/text()}"/>
      </xsl:if>
      <xsl:if test="$gnd-dataset/rdf:RDF/*/gndo:placeOfBirth/rdf:Description/@rdf:about">
         <gndo:placeOfBirth gndo:ref="{$gnd-dataset/rdf:RDF/*/gndo:placeOfBirth/rdf:Description/@rdf:about}"><xsl:value-of select="$gnd-dataset/rdf:RDF/*/gndo:placeOfBirth/rdf:Description/rdfs:label/text()"/></gndo:placeOfBirth>
      </xsl:if>
      <xsl:if test="$gnd-dataset/rdf:RDF/*/gndo:placeOfDeath/rdf:Description/@rdf:about">
         <gndo:placeOfDeath gndo:ref="{$gnd-dataset/rdf:RDF/*/gndo:placeOfDeath/rdf:Description/@rdf:about}"><xsl:value-of select="$gnd-dataset/rdf:RDF/*/gndo:placeOfDeath/rdf:Description/rdfs:label/text()"/></gndo:placeOfDeath>
      </xsl:if>
      <xsl:call-template name="get-basic-info">
         <xsl:with-param name="gnd-dataset" select="$gnd-dataset"/>
      </xsl:call-template>
      <xsl:call-template name="get-cst-data"/>
   </xsl:template>
   
   <xsl:template name="process-work-entry">
      <xsl:param name="gnd-dataset"/>
      <xsl:if test="$gnd-dataset/rdf:RDF/rdf:Description/gndo:preferredNameForTheWork">
         <gndo:preferredName><xsl:value-of select="$gnd-dataset/rdf:RDF/rdf:Description/gndo:preferredNameForTheWork"/></gndo:preferredName>
      </xsl:if>
      <xsl:if test="$gnd-dataset/rdf:RDF/rdf:Description/gndo:firstAuthor">
         <gndo:author gndo:ref="{$gnd-dataset/rdf:RDF/rdf:Description/gndo:firstAuthor/@rdf:resource}"/>
      </xsl:if>
      <xsl:if test="$gnd-dataset/rdf:RDF/rdf:Description/gndo:firstComposer">
         <gndo:author gndo:ref="{$gnd-dataset/rdf:RDF/rdf:Description/gndo:firstComposer/@rdf:resource}"/>
      </xsl:if>
      <xsl:if test="$gnd-dataset/rdf:RDF/rdf:Description/gndo:librettist
         and $gnd-dataset/rdf:RDF/rdf:Description/gndo:librettist/@rdf:resource != $gnd-dataset/rdf:RDF/rdf:Description/gndo:firstComposer/@rdf:resource">
         <gndo:author gndo:ref="{$gnd-dataset/rdf:RDF/rdf:Description/gndo:librettist/@rdf:resource}"/>
      </xsl:if>
      <xsl:for-each select="$gnd-dataset/rdf:RDF/rdf:Description/gndo:biographicalOrHistoricalInformation">
         <gndo:biographicalOrHistoricalInformation><xsl:value-of select="."/></gndo:biographicalOrHistoricalInformation>
      </xsl:for-each>
   </xsl:template>
   
</xsl:stylesheet>