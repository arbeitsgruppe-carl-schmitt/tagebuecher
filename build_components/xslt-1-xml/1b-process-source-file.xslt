<xsl:stylesheet exclude-result-prefixes="my xs" version="3.0" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:my="http://example.com/my"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output indent="no" method="xml"/>
    <xsl:mode on-no-match="shallow-copy" use-accumulators="#all"/>
    <xsl:function name="my:compute-facs" as="xs:string">
        <xsl:param name="scan-number" as="xs:integer"/>
        <xsl:sequence select="replace($scan-link, '\{page\}', xs:string($scan-number))"/>
    </xsl:function>
     
    <xsl:param name="book" select="book"/>
    <xsl:param name="first-page" select="first-page"/>
    <xsl:param name="page-increment" select="page-increment"/>
    <xsl:param name="scan-link" select="scan-link"/>

    <xsl:accumulator as="xs:integer" initial-value="0" name="lbCount">
        <xsl:accumulator-rule match="lb" select="$value + 1"/>
        <xsl:accumulator-rule match="pb" select="0"/>
    </xsl:accumulator>

    <xsl:accumulator as="xs:integer"
        initial-value="if ($first-page = '') then 0 else xs:integer($first-page)"
        name="scan-number">
        <xsl:accumulator-rule match="front" select="$value"/>
        <xsl:accumulator-rule match="div[@type = 'inlaid-sheet' or @type = 'open-book']"
            select="if ($page-increment = '') then $value else $value + xs:integer($page-increment)"/>
    </xsl:accumulator>
    
    <xsl:template match="div[@type = 'inlaid-sheet' or @type = 'open-book']">
        <xsl:element name="milestone">
            <xsl:if test="$first-page != '' and $page-increment != '' and $scan-link != ''">
                <xsl:attribute name="facs" 
                    select="my:compute-facs(accumulator-after('scan-number'))"/>
            </xsl:if>
            <xsl:attribute name="type" select="@type"/>
            <xsl:attribute name="unit" select="'image'"/>
            <xsl:attribute name="xml:id" select="@xml:id"/>
        </xsl:element>
        <xsl:apply-templates select="*"/>
    </xsl:template>
    
    <xsl:template match="front">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:if test="$first-page != '' and $page-increment != '' and $scan-link != ''">
                <xsl:attribute name="facs" 
                    select="my:compute-facs(accumulator-after('scan-number'))"/>
            </xsl:if>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
    

    <xsl:template match="lb">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:attribute name="n" select="format-number(accumulator-after('lbCount'), '0')"/>
        </xsl:copy>
    </xsl:template>
        
    <xsl:template match="/TEI/text//text()[not(parent::c[@type='persistent-hyphen']) and matches(normalize-space(.), '[a-z]*-$')]">
        <xsl:variable name="str" as="xs:string" select="normalize-space(.)"/>
        <xsl:choose>
            <xsl:when
                test="./following::node()[1][local-name() = 'lb']/following::text()[1][matches(normalize-space(.), '^[a-z]')]">
                <xsl:analyze-string regex="-$" select="$str">
                    <xsl:matching-substring>
                        <c type="ephemeral-hyphen"><xsl:copy-of select="."/></c>
                    </xsl:matching-substring>
                    <xsl:non-matching-substring>
                        <xsl:copy-of select="."/>
                    </xsl:non-matching-substring>
                </xsl:analyze-string>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
