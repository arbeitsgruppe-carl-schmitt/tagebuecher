<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet exclude-result-prefixes="entx xs" version="3.0"
   xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
   xmlns:entx="https://sub.uni-goettingen.de/met/standards/entity-xml#"
   xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
   xmlns:tei="http://www.tei-c.org/ns/1.0"
   xmlns:xs="http://www.w3.org/2001/XMLSchema"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:mode on-no-match="shallow-copy"/>
   <xsl:param name="authors-list" select="authors-list"/>
   <xsl:variable name="authors-ids" as="xs:string*" select="tokenize($authors-list, ',')"/>
   <xsl:output indent="yes" method="xml"/>
   
   <xsl:variable name="existing-ids" as="xs:string*">
      <xsl:for-each select="/entityXML/collection/data/list/person[starts-with(@gndo:uri, 'https://d-nb.info/gnd/')]">
         <xsl:copy-of select="./substring-after(@gndo:uri, 'https://d-nb.info/gnd/')"/>
      </xsl:for-each>
   </xsl:variable>
   
   <xsl:variable name="persons" as="item()*">
      <xsl:copy-of select="/entityXML/collection/data/list/person"/>
      <xsl:for-each select="tokenize($authors-list, ',')">
         <xsl:variable name="this-id" select="." as="xs:string"/>
         <xsl:if test="not($existing-ids[.=$this-id])">
            <xsl:message>Adding author with gndo:uri="https://d-nb.info/gnd/<xsl:value-of select="$this-id"/> to persons file.</xsl:message>
            <person gndo:uri="https://d-nb.info/gnd/{$this-id}" xml:id="__GND_{$this-id}" xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#"/>
         </xsl:if>
      </xsl:for-each>
   </xsl:variable>
   
   <xsl:template match="/entityXML/collection/data/list">
      <xsl:copy>
         <xsl:apply-templates select="@*"/>
         <xsl:copy-of select="$persons"/>
      </xsl:copy>
   </xsl:template>
</xsl:stylesheet>
