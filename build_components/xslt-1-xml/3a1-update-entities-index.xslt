<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="xs" version="3.0"
   xmlns:entx="https://sub.uni-goettingen.de/met/standards/entity-xml#"
   xmlns:tei="http://www.tei-c.org/ns/1.0"
   xmlns:xs="http://www.w3.org/2001/XMLSchema"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:param name="entities-config-data-path" select="entities-config-data-path"/>
   <xsl:param name="entity" select="entity"/>
   <xsl:param name="entities-file-old" select="entities-file-old"/>
   <xsl:param name="gnd-entities-list-path" select="gnd-entities-list-path"/>
   <xsl:variable as="node()" name="config-data" select="document($entities-config-data-path)/entities"/>
   <xsl:variable as="node()" name="entity-config-data" select="$config-data/*[local-name() = $entity]"/>
   <xsl:output indent="yes" method="xml"/>
   <xsl:template match="/">
      <xsl:processing-instruction name="xml-model">href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"</xsl:processing-instruction>
      <xsl:processing-instruction name="xml-model">href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"</xsl:processing-instruction>
      <xsl:processing-instruction name="xml-stylesheet">type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.css" title="entityxml" alternate="no"</xsl:processing-instruction>
      <xsl:processing-instruction name="xml-stylesheet">type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"</xsl:processing-instruction>
      <entityXML xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#"
         xmlns:cst="https://carlschmitt.bio/entityxml/terms#"
         xmlns:dc="http://purl.org/dc/elements/1.1/"
         xmlns:dnb="https://www.dnb.de"
         xmlns:geo="http://www.opengis.net/ont/geosparql#"
         xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
         xmlns:skos="http://www.w3.org/2004/02/skos/core#"
         xmlns:wgs84="http://www.w3.org/2003/01/geo/wgs84_pos#">
         <xsl:variable name="A_old-entries">
            <xsl:choose>
               <xsl:when test="ends-with($entities-file-old, '_old')">
                  <xsl:copy-of select="document($entities-file-old)/entx:entityXML/entx:collection/entx:data/entx:list/entx:*[local-name()=$entity and @xml:id]"/>
               </xsl:when>
               <xsl:otherwise/>
            </xsl:choose>
         </xsl:variable>
         <xsl:variable name="B_current-entries">
            <xsl:for-each select="/tei:teiCorpus/tei:TEI/tei:text/tei:body//tei:rs[@type = $entity and (@ref[starts-with(., '#')])
               and not(starts-with(., '#zotero/'))
               and not(preceding::tei:rs[@type = $entity]/@ref = @ref)]">
               <xsl:element name="{$entity}">
                  <xsl:choose>
                     <xsl:when test="matches(@ref, '^#__GND_[0-9X-]*$')">
                        <xsl:attribute name="xml:id" select="'__GND_' || substring-after(@ref, '#__GND_')"/>
                        <xsl:attribute name="gndo:uri" select="'https://d-nb.info/gnd/' || substring-after(@ref, '#__GND_')"/>
                     </xsl:when>
                     <xsl:when test="starts-with(@ref, '#')">
                        <xsl:attribute name="xml:id" select="substring-after(@ref, '#')"/>
                        <xsl:copy-of select="$entity-config-data/entry-elements/*"/>
                     </xsl:when>
                  </xsl:choose>
               </xsl:element>
            </xsl:for-each>
         </xsl:variable>
         <xsl:variable name="count_b" select="count($B_current-entries/*)"/>
         <xsl:variable name="C_merged-entries">
            <xsl:for-each select="$B_current-entries/*">
               <xsl:variable name="this-id" select="@xml:id"/>
               <xsl:choose>
                  <xsl:when test="$A_old-entries/*[@xml:id = $this-id]">
                     <xsl:copy-of select="$A_old-entries/*[@xml:id = $this-id]"/>
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:copy-of select="."/>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:for-each>
         </xsl:variable>
         <xsl:variable name="count_c" select="count($C_merged-entries/*)"/>
         <xsl:message><xsl:value-of select="$entity || 's index count: ' || $count_c"/></xsl:message>
         <xsl:result-document href="{$gnd-entities-list-path}" method="text">
            <xsl:for-each select="$C_merged-entries/*">
               <xsl:if test="@gndo:uri and matches(@gndo:uri, '^https://d-nb.info/gnd/[0-9X-]+$')">
                  <xsl:value-of select="tokenize(@gndo:uri, '/')[last()] || '&#10;'"/>
               </xsl:if>
            </xsl:for-each>
         </xsl:result-document>
         <collection>
            <metadata>
               <title>
                  <xsl:value-of select="$entity-config-data/metadata/title/text()"/>
               </title>
               <abstract>
                  <xsl:value-of select="$entity-config-data/metadata/abstract/text()"/>
               </abstract>
               <provider id="{$config-data/provider/@id}"/>
               <revision status="{$entity-config-data/metadata/revision/@status}">
                  <xsl:value-of select="$entity-config-data/metadata/revision/text()"/>
               </revision>
            </metadata>
            <data>
               <list>
                  <xsl:copy-of select="$C_merged-entries"/>
               </list>
            </data>
         </collection>
      </entityXML>
   </xsl:template>
</xsl:stylesheet>
