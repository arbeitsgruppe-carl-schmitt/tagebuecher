<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    version="3.0"
    exclude-result-prefixes="tei xs"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <!-- <xsl:output indent="no" method="xml"/> -->
    <xsl:mode on-no-match="shallow-copy"/>
    
    <xsl:template match="date[not(@type='entry-date')]">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="div[@type='calendar-entry' or @type='diary-entry' or @type='excerpt' or @type='letter-draft']">
        <xsl:copy>
            <xsl:variable name="subtype">
                <xsl:choose>
                    <xsl:when test="@type='calendar-entry'">
                        <xsl:value-of select="'calendar'"/>
                    </xsl:when>
                    <xsl:when test="@type='diary-entry'">
                        <xsl:value-of select="'diary'"/>
                    </xsl:when>
                    <xsl:when test="@type='excerpt'">
                        <xsl:value-of select="'excerpt'"/>
                    </xsl:when>
                    <xsl:when test="@type='letter-draft'">
                        <xsl:value-of select="'letter-draft'"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:variable>
            <xsl:attribute name="type" select="'entry'"/>
            <xsl:attribute name="subtype" select="$subtype"/>
            <xsl:apply-templates select="@*[not(local-name() = 'type')] | node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="seg[child::rs and count(child::node()) = 1]
        | rs[child::seg and count(child::node()) = 1]">
        <xsl:call-template name="create_simple_rs_el"/>
    </xsl:template>

    <xsl:template match="@ref[starts-with(., 'https://d-nb.info/gnd/')]">
        <xsl:attribute name="ref" select="'#__GND_' || substring-after(., 'https://d-nb.info/gnd/')"/>
    </xsl:template>
    
    <xsl:template name="create_simple_rs_el">
        <xsl:element name="rs" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates select="child::*/@* | child::*/node()"/>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>
