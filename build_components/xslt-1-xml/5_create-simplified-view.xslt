<xsl:stylesheet
    version="3.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output indent="no" method="xml"/>
    <xsl:mode on-no-match="shallow-copy"/>

    <xsl:template match="/teiCorpus/teiHeader/fileDesc/sourceDesc">
        <xsl:copy>
            <xsl:copy-of select="document('/data/cst/git-repos/tagebuecher/machine-data/xml-results/entities-maps/persons.xml')/listPerson"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
