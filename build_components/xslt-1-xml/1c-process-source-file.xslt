<xsl:stylesheet version="3.0" xmlns="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output indent="no" method="xml"/>
    <xsl:mode on-no-match="shallow-copy"/>

    <xsl:template match="ab//lb | head//lb | lg//lb | note//lb">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:if test="ancestor::*[local-name() = 'ab' or local-name() = 'lg' or local-name() = 'note'][1]/@xml:id">
                <xsl:variable name="thisParentId" select="ancestor::*[local-name() = 'ab' or local-name() = 'lg' or local-name() = 'note'][1]/@xml:id"/>
                <xsl:choose>
                    <xsl:when test="not(preceding::lb[ancestor::*[@xml:id = $thisParentId]])">
                        <xsl:attribute name="type" select="'first'"/>
                    </xsl:when>
                    <xsl:when test="preceding-sibling::*[1][local-name()='c' and @type='ephemeral-hyphen']">
                        <xsl:attribute name="type" select="'in-word'"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:if>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
