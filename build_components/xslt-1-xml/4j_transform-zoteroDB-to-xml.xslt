<xsl:stylesheet exclude-result-prefixes="xs fn" version="3.0" xmlns="http://www.tei-c.org/ns/1.0" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="yes" method="xml"/>
    <xsl:param as="xs:string" name="json-file-path"/>
    <xsl:template name="init">
        <xsl:variable name="json-text" select="unparsed-text($json-file-path)"/>
        <xsl:variable name="json-xml" select="json-to-xml($json-text)"/>
        <listBibl>
            <xsl:for-each select="$json-xml/fn:array/fn:map/fn:map[@key = 'data']">
                <xsl:variable name="item-type" select="fn:string[@key = 'itemType']"/>
                <bibl type="{$item-type}" xml:id="__ZOTERO_{fn:string[@key='key']}">
                    <title type="main">
                        <xsl:value-of select="fn:string[@key = 'title']"/>
                    </title>
                    <xsl:for-each select="fn:array[@key = 'creators']/fn:map[child::fn:string[@key = 'creatorType']/text() = 'author']">
                        <xsl:call-template name="get-author"/>
                    </xsl:for-each>
                    <xsl:if test="$item-type = 'artwork'">
                        <xsl:for-each select="fn:array[@key = 'creators']/fn:map[child::fn:string[@key = 'creatorType']/text() = 'artist']">
                            <xsl:call-template name="get-author"/>
                        </xsl:for-each>
                        <xsl:if test="fn:string[@key = 'artworkMedium']/text()">
                            <note type="artwork-medium">
                                <xsl:value-of select="fn:string[@key = 'artworkMedium']"/>
                            </note>
                        </xsl:if>
                        <xsl:if test="fn:string[@key = 'archive']/text()">
                            <note type="artwork-repository">
                                <xsl:value-of select="fn:string[@key = 'archive']/text()"/>
                            </note>
                        </xsl:if>
                        <xsl:if test="fn:string[@key = 'artworkSize']/text()">
                            <note type="artwork-size">
                                <xsl:value-of select="fn:string[@key = 'artworkSize']"/>
                            </note>
                        </xsl:if>
                    </xsl:if>
                    <xsl:if test="$item-type = 'book'">
                        <xsl:call-template name="get-edition"/>
                        <xsl:call-template name="get-editors"/>
                        <xsl:call-template name="get-pub-place"/>
                        <xsl:call-template name="get-publisher"/>
                        <xsl:call-template name="get-series-title"/>
                        <xsl:call-template name="get-series-num"/>
                    </xsl:if>
                    <xsl:if test="$item-type = 'book' or $item-type = 'letter' or $item-type = 'artwork'">
                        <xsl:call-template name="get-date"/>
                    </xsl:if>
                    <xsl:if test="$item-type = 'bookSection' or $item-type = 'journalArticle'">
                        <bibl type="containing-publication">
                            <xsl:choose>
                                <xsl:when test="$item-type = 'bookSection'">
                                    <xsl:if test="fn:string[@key = 'bookTitle']/text()">
                                        <title type="main">
                                            <xsl:value-of select="fn:string[@key = 'bookTitle']"/>
                                        </title>
                                    </xsl:if>
                                    <xsl:call-template name="get-editors"/>
                                    <xsl:call-template name="get-pub-place"/>
                                    <xsl:call-template name="get-publisher"/>
                                    <xsl:call-template name="get-series-title"/>
                                    <xsl:call-template name="get-series-num"/>
                                </xsl:when>
                                <xsl:when test="$item-type = 'journalArticle'">
                                    <xsl:call-template name="get-pub-title"/>
                                    <xsl:call-template name="get-volume"/>
                                    <xsl:call-template name="get-issue"/>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:call-template name="get-pages"/>
                            <xsl:call-template name="get-date"/>
                        </bibl>
                    </xsl:if>
                    <xsl:if test="$item-type = 'letter'">
                        <xsl:if test="fn:string[@key = 'archive']/text()">
                            <note type="archive">
                                <xsl:value-of select="fn:string[@key = 'archive']"/>
                            </note>
                        </xsl:if>
                        <xsl:if test="fn:string[@key = 'archiveLocation']/text()">
                            <note type="archive-location">
                                <xsl:value-of select="fn:string[@key = 'archiveLocation']"/>
                            </note>
                        </xsl:if>
                    </xsl:if>
                    <xsl:if test="$item-type = 'magazineArticle'">
                        <bibl type="containing-publication">
                            <xsl:call-template name="get-pub-title"/>
                            <xsl:call-template name="get-date"/>
                            <xsl:call-template name="get-volume"/>
                            <xsl:call-template name="get-issue"/>
                            <xsl:call-template name="get-pages"/>
                        </bibl>
                    </xsl:if>
                    <xsl:if test="$item-type = 'newspaperArticle'">
                        <bibl type="containing-publication">
                            <xsl:call-template name="get-pub-title"/>
                            <xsl:call-template name="get-date"/>
                            <xsl:call-template name="get-pages"/>
                        </bibl>
                    </xsl:if>
                </bibl>
            </xsl:for-each>
        </listBibl>
    </xsl:template>
    <xsl:template name="get-author">
        <author>
            <xsl:choose>
                <xsl:when test="fn:string[@key = 'firstName']/text() and fn:string[@key = 'lastName']/text()">
                    <xsl:value-of select="fn:string[@key = 'firstName'] || ' ' || fn:string[@key = 'lastName']"/>
                </xsl:when>
                <xsl:when test="fn:string[@key = 'name']/text()">
                    <xsl:value-of select="fn:string[@key = 'name']"/>
                </xsl:when>
            </xsl:choose>
        </author>
    </xsl:template>
    <xsl:template name="get-date">
        <xsl:if test="fn:string[@key = 'date']/text()">
            <date>
                <xsl:value-of select="fn:string[@key = 'date'][1]"/>
            </date>
        </xsl:if>
    </xsl:template>
    <xsl:template name="get-edition">
        <xsl:if test="fn:string[@key = 'edition']/text()">
            <edition>
                <xsl:value-of select="fn:string[@key = 'edition']"/>
            </edition>
        </xsl:if>
    </xsl:template>
    <xsl:template name="get-editors">
        <xsl:for-each select="fn:array[@key = 'creators']/fn:map[child::fn:string[@key = 'creatorType']/text() = 'editor']">
            <editor>
                <xsl:value-of select="fn:string[@key = 'firstName'] || ' ' || fn:string[@key = 'lastName']"/>
            </editor>
        </xsl:for-each>
    </xsl:template>
    <xsl:template name="get-issue">
        <xsl:if test="fn:string[@key = 'issue']/text()">
            <biblScope unit="issue">
                <xsl:value-of select="fn:string[@key = 'issue']"/>
            </biblScope>
        </xsl:if>
    </xsl:template>
    <xsl:template name="get-volume">
        <xsl:if test="fn:string[@key = 'volume']/text()">
            <biblScope unit="volume">
                <xsl:value-of select="fn:string[@key = 'volume']"/>
            </biblScope>
        </xsl:if>
    </xsl:template>
    <xsl:template name="get-pages">
        <xsl:if test="fn:string[@key = 'pages']/text()">
            <biblScope unit="page">
                <xsl:value-of select="fn:string[@key = 'pages']"/>
            </biblScope>
        </xsl:if>
    </xsl:template>
    <xsl:template name="get-pub-place">
        <xsl:if test="fn:string[@key = 'place']/text()">
            <pubPlace>
                <xsl:value-of select="fn:string[@key = 'place'][1]"/>
            </pubPlace>
        </xsl:if>
    </xsl:template>
    <xsl:template name="get-pub-title">
        <xsl:if test="fn:string[@key = 'publicationTitle']/text()">
            <title type="main">
                <xsl:value-of select="fn:string[@key = 'publicationTitle']"/>
            </title>
        </xsl:if>
    </xsl:template>
    <xsl:template name="get-publisher">
        <xsl:if test="fn:string[@key = 'publisher']/text()">
            <publisher>
                <xsl:value-of select="fn:string[@key = 'publisher']"/>
            </publisher>
        </xsl:if>
    </xsl:template>
    <xsl:template name="get-series-num">
        <xsl:if test="fn:string[@key = 'seriesNumber']/text()">
            <num type="series">
                <xsl:value-of select="fn:string[@key = 'seriesNumber']"/>
            </num>
        </xsl:if>
    </xsl:template>
    <xsl:template name="get-series-title">
        <xsl:if test="fn:string[@key = 'series']/text()">
            <title type="series">
                <xsl:value-of select="fn:string[@key = 'series']"/>
            </title>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
