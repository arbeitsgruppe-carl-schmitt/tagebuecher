# Carl Schmitt Diaries
*Digital Edition. Ed. by \
Philip Manow and Florian Meinel*

*Funded by the German Research Foundation (Deutsche Forschungsgemeinschaft, DFG) \
Located at the University of Göttingen, Germany, 2021-2024*

***

With the generous support of the [Deutsche Forschungsgemeinschaft](https://www.dfg.de), we are working to build a comprehensive edition of the diaries of the German lawyer and political theorist Carl Schmitt (1888-1985). His diaries, written in an almost forgotten shorthand and preserved in his papers at the [State Archives of North-Rine Westfalia at Duisburg](https://www.archive.nrw.de/en/department-rhineland-duisburg), cover a period of six decades, from the late years of the Empire and the Weimar Republic to the 1960s and the Federal Republic. Ever since the first volumes were published, they are counted among the most important diary works written in German language. 

We intend to publish all research data and source material, including high-resolution scans of Schmitt's diaries, our TEI XML source code, and the transformation software. Eventually, this project will make the entire body of Schmitt's diaries available through one user interface in a free and open source manner.

The most recent preliminary release of the edition's HTML interface is available at [carlschmitt.bio](https://carlschmitt.bio).

The edition's complete TEI XML text is available for download [in a ZIP package](https://carlschmitt.bio/downloads/cst-primary-text-package.zip)).

***

[TOC]

## Getting the source code
The TEI XML source code of this edition and the scripts to transform it into a website or PDF files are kept in different git repositories. To clone the [public TEI XML repository](https://gitlab.com/arbeitsgruppe-carl-schmitt/public-xml), run
```bash
git clone https://gitlab.com/arbeitsgruppe-carl-schmitt/public-xml.git
```
However, if you need to compile the TEI XML source code to its machine edited and assembled form and if you need to transform it into target formats like HTML, you can do this on your local machine with a container Linux instance. In this case, don't download the source code repository by itself, but proceed as described below. 

## Build process

You can start from scratch and build a version of the edition on your local device, including the enhanced master TEI XML files and the static HTML website. A main benefit of this approach is that you can see the effects of your changes to source code and scripts before actually committing anything. This can prevent you from committing code that would create erroneous or invalid results. The build process has been tested on Linux and MacOS. On Windows, you can use [WSL2 Linux](https://learn.microsoft.com/en-us/windows/wsl/install). This description aims to create the necessary run time environment in a [Podman Linux Container](https://podman.io/). To do this, it would be helpful to have a basic unterstanding of virtual machines, container operating systems and Unix command line usage.

### Linux perequisites

First, make sure you have an up-to-date GNU/Linux system with Git and Podman installed. To install these packages, on **Fedora Linux** you can run:
```bash
sudo dnf upgrade && sudo dnf install -y git podman
```
Alternatively, on **Debian Linux** this would be:
```bash
sudo apt-get update && sudo apt-get upgrade && sudo apt-get install -y git podman
```
Create a new folder to hold our project data (adapt the project path to your needs):
```bash
project_path="$HOME/data/cst/git-repos" && mkdir -p "$project_path"
```

### MacOS perequisites
On **MacOS**, first install the brew packet manager [as described here](https://brew.sh/) and then the git and podman packages.
```bash
brew upgrade && brew install git podman
```
Create a new folder for our project data:
```bash
project_path=$HOME/Data/cst/git-repos && mkdir -p $project_path
```
Let's create a new virtual Linux machine to run our container on. Adjust the specs to your liking. (We need to mount a volume to make our project data available to the VM.)
```bash
podman machine init --cpus=6 --memory=4096 --now --volume=$HOME/Data/cst:/data/cst cst-machine
```
We also need to set the destination for podman services:
```bash
podman system connection default cst-machine
```
You can later stop and restart this virtual machine with `podman machine stop cst-machine` and `podman machine start cst-machine`. You can only use podman containers on MacOS when your podman machine is up and running.

### Downloading the main repository
Enter the project folder and clone the main repository with all submodules.
```bash
cd "$project_path" && git clone --recurse-submodules https://gitlab.com/arbeitsgruppe-carl-schmitt/tagebuecher.git
```
**Optional**: If you are a member of the Arbeitsgruppe Carl Schmitt and have access, also clone the internal repository:
```bash
git clone https://gitlab.com/arbeitsgruppe-carl-schmitt/internal-xml.git
```
**Optional**: Later, when working with the data, to update all data from all repositories, you can run:
```bash
cd "$project_path"/tagebuecher
make pull
```
(This means, you can just run `make pull` within the main directory of this project to update everything at once.)

### Create a new Podman image and Container Linux instance

In this example, we will call our image `cst-img` and the container instance `cst-cont`. First cd to the main repo:
```bash
cd "$project_path/tagebuecher"
```
Build the container image (this may take a few minutes):
```bash
podman build --file=podman/Containerfile --tag=cst-img
```
Create a container instance of our new image and run it in detached mode. On a **Linux machine**, run
```bash
podman run -it -d -p=127.0.0.1:8080:80 --name=cst-cont -v="$project_path":/data/cst/git-repos:Z localhost/cst-img
```
Alternatively, on **MacOS**, run
```bash
podman run -it -d -p=127.0.0.1:8080:80 --name=cst-cont -v=/data/cst/git-repos:/data/cst/git-repos localhost/cst-img
```
The container with our build environment has access to the project directory of our host machine, so we can comfortably edit settings there to configure the build process and we can also access the build process results within this directory. 

(We are adding the Z flag to the -v/--volume option on Linux systems to avoid trouble with Selinux policies, but we don't need this flag on MacOS systems.)
You can change the port number `8080` for the host system to other values if you wish.

### Config file

Copy the sample build config file if there is no config file yet:
```bash
[[ -f sh/build-config.json ]] || cp sh/build-config-sample.json sh/build-config.json 
```

**Optional**: You can customize the build settings by editing the config file `sh/build-config.json`. Skip this section to just go with the default settings.

You can edit this file with a text editor, e.g.
```bash
nano sh/build-config.json
```
If you have an internal TEI XML source code repository you would like to use, add its git URL to the file `build-config.json` as the value for the key `.build-config.json`. For members of the Arbeitsgruppe Carl Schmitt this would look like this:
```json
"internal_repo_url": "https://gitlab.com/arbeitsgruppe-carl-schmitt/internal-xml.git"
```
If you own a proprietary license of Font Awesome, set `.use_fontawesome_pro` to `true`, so proprietary icons will be used in the edition's HTML build:
```json
"font_awesome_url": true
```
Within the main repository, copy a version of Font Awesome Pro (>= 6.2.1) into the folder `/site_components/fonts/fontawesome6/pro`. We just need the subfolders `css` and `webfonts` from the FA package. The command
```bash
ls site_components/fonts/fontawesome6/pro
```
should yield the result `css webfonts`.

## Final build preparations

Next, we need to download additional resources like images, fonts, etc.
```bash
podman exec cst-cont make install
```

### Creating a build of the edition

Finally we are ready to create the first local website of the digital edition. Run 
```bash
podman exec cst-cont make site
```
The build process will create new folders with machine-generated data in your main git repository: `$project_path/tagebuecher/machine-data`.

You can access the newly created website with a webbrowser from your host machine. Use a webbrowser like Google Chrome or Mozilla Firefox to visit the URL [cst.localhost:8080](http://cst.localhost:8080). It might be necessary to feed the URL two times to the browser, if he doesn't swallow the new address on first try. Make sure the port number `:8080` doesn't get lost.

Run `podman exec cst-cont make site` whenever you want to create a new local website.

### Available make commands
Run these commands as `podman exec cst-cont [COMMAND]`, or 'within' the container after attaching it to the terminal with `podman attach cst-cont`:
- `make clean` -- Delete all machine-written data created by the above `make` commands.
- `make clean-local` -- Delete all machine-written data created by the above `make` commands, except data that has been downloaded from the Deutsche Nationalbibliothe and our Zotero database.
- `make index` -- Create/update the machine written index of XML entities stored in the submodule `xml/entities`.
- `make release` -- Create a new minified and compressed website using current source data. Available on the host machine at [cst-release.localhost:8080](http://cst-release.localhost:8080)
- `make site` -- Create the edition's website using current source data. Available on the host machine at [cst.localhost:8080](http://cst.localhost:8080)
- `make sync` -- Sync the private source code repo with the public source code repo and push changes to the internal repo. Data in the public repo will overwrite data in the private one. Needs git credentials to work. Only use this when you know what you are doing.
- `make xml_page-view` -- Create the edition's machine written TEI XML code, including the full text in one file (`machine-data/xml-results/[REPOSITORY NAME]/edition`). Useful for checking the data's validity and XPATH queries.

### Working with the build container

To start, stop and delete the container at a later time, we can run:
```bash
# Stop the container
podman stop cst-cont
# Start the container
podman start cst-cont
# Delete the container, e.g. to rebuild from scratch:
podman rm cst-cont
```

## Keep your system up to date
**Podman Linux container**
```bash
podman exec cst-cont dnf upgrade -y
```
**Fedora Linux host**
```bash
sudo dnf upgrade -y
```
**Debian Linux host**
```bash
sudo apt-get update && sudo apt-get upgrade -y
```
**MacOS host**
```zsh
brew upgrade
```
Don't worry about your Podman Machine VM on MacOS, it is by default running Fedora CoreOS and will automatically update itself every 2 weeks.

## Participation

We greatly appreciate support from all members of the worldwide academic community of Carl Schmitt scholars. We will be very grateful for any corrections and suggestions regarding our transcriptions, commentary, and editorial standard. Feel free to create an issue [in our issue board](https://gitlab.com/arbeitsgruppe-carl-schmitt/public-xml/-/issues), which requires a GitLab account. We will accept pull requests at any time. If you hesitate to sign up for Gitlab, please reach out using any of the addresses below. 

## Contact

**Prof. Dr. Philip Manow**\
Universität Bremen\
SOCIUM Forschungszentrum Ungleichheit und Sozialpolitik\
Unicom-Gebäude\
Mary-Somerville-Straße 7\
28359 Bremen\
Germany\
manow@uni-bremen.de

**Prof. Dr. Florian Meinel**\
Institut für Grundlagen des Rechts\
Abteilung für Staatstheorie, Politische Wissenschaften und Vergleichendes Staatsrecht\
Nikolausberger Weg 17\
37073 Göttingen\
Germany\
florian.meinel@jura.uni-goettingen.de

***

*For technology related questions*\
martin.hinze@jura.uni-goettingen.de
